README.md

---

- [Installazione APP da Repository](#installazione-app-da-repository)
- [NODE](#node)
  - [Installazione](#installazione)
  - [Angular CLI e PM2](#angular-cli-e-pm2)
- [REDIS](#redis)
  - [Installazione](#installazione-1)
  - [Aggiungere autenticazione](#aggiungere-autenticazione)
- [NGINX](#nginx)
  - [Installazione](#installazione-2)
  - [Configurazione NGINX](#configurazione-nginx)
  - [File di configurazione per api.mutuiamo.it](#file-di-configurazione-per-apimutuiamoit)
  - [File di configurazione per advisorpanel.mutuiamo.it](#file-di-configurazione-per-advisorpanelmutuiamoit)
- [SSL](#ssl)
  - [Installazione certificato](#installazione-certificato)
- [PM2](#pm2)
  - [Installazione](#installazione-3)
  - [Comandi utili](#comandi-utili)
- [MONGODB](#mongodb)
  - [Installazione](#installazione-4)
  - [Abilitazione autenticazione](#abilitazione-autenticazione)
  - [Connessione](#connessione)
  - [Importare collezioni MONGODB](#importare-collezioni-mongodb)
- [MYSQL](#mysql)
  - [Installazione](#installazione-5)
  - [Connessione](#connessione-1)
- [Utenza per CI/CD](#utenza-per-cicd)

# Installazione APP da Repository

```
cd /usr/share/nginx
git clone git@gitlab.rete.farm:mutui/products/advisor-panel.git
git clone git@gitlab.rete.farm:mutui/services/mobile-service.git
```

Creazione build per advisor panel

```
cd advisor-panel
git checkout release
npm i
ng build
```

```
cd ../mobile-service
git checkout release
npm i
touch .env
```

Caricare il file firebase.json

```
touch src/certificates/firebase.json
```

Popolare il file con le variabili di ambiente

```
SENDGRID_API_KEY=
FROM_EMAIL=app@mutuiamo.it

RATES_ENDPOINT=https://www.immobiliare.it/mutui/survey/loanvalues
LEAD_ENDPOINT=https://www.immobiliare.it/mutui/survey/submit?theme=mutuiamo
CLIENT_URL=advisorpanel.mutuiamo.it

REDIS_HOST=127.0.0.1
REDIS_PORT=6379
REDIS_PASSWORD=

IP_REQUEST_EXPIRATION=
IP_REQUEST_OTP_MAX=
IP_REQUEST_AUTH_MAX=

MONGO_DB_USERNAME=mutuiamo
MONGO_DB_PASSWORD=mutuiamo
MONGO_DB_HOST=localhost
MONGO_DB_PORT=27017
MONGO_DB_DATABASE=mutuiamo

MYSQL_USERNAME=mutuiamo
MYSQL_PASSWORD=mutuiamo
MYSQL_HOST=localhost
MYSQL_PORT=3306
MYSQL_DATABASE=mutuiamo

NODE_PORT=3000
NODE_ENV=production

SEND_SMS=ON
```

# NODE

## Installazione

```
node -v
v14.18.2
```

Se non abbiamo questa risposta, installare come segue:

```
yum -y install curl
curl -sL https://rpm.nodesource.com/setup_14.x | bash -
yum install -y nodejs
```

## Angular CLI e PM2

```
npm install -g @angular/cli@12
npm i -g pm2
```

# REDIS

## Installazione

```
yum install redis
systemctl start redis
systemctl enable redis
```

## Aggiungere autenticazione

Aggiungere password allo store

```
vi /etc/redis.conf
```

Cercare la sezione SECURITY

```
SECURITY
# requirepass foobared
```

Togliere il commento e cambiare `foobared` con la password scelta ad esempio:

```
SECURITY
requirepass 2411808e9ca6762
```

Riavviare redis

```
systemctl restart redis
```

# NGINX

## Installazione

```
yum install epel-release
yum install nginx
```

Far partire NGINX e caricarlo all'avvio della macchina

```
systemctl start nginx
systemctl enable nginx
```

## Configurazione NGINX

Creare le cartelle per contenere i file di configurazione degli host

```
mkdir /etc/nginx/sites-enabled
mkdir /etc/nginx/sites-available
```

Arricchire il file di configurazione generale

```
vi /etc/nginx/nginx.conf
```

alla fine di http

```
http {

    ...

    include /etc/nginx/sites-enabled/\*.conf;
    server_names_hash_bucket_size 64;
    client_max_body_size 15M;

}
```

Segnarsi IP interno ed esterno della macchina

```
hostname -I
```

## File di configurazione per api.mutuiamo.it

Creare il file di configurazione per l'app dei servizi

```
vi /etc/nginx/sites-available/api.mutuiamo.it.conf
```

deve contenere il seguente contenuto, sostituendo [INTERNAL_IP] con il valore trovato prima.

```
upstream node_app {
server [INTERNAL_IP]:3000;
}

upstream websocket_server {
server [INTERNAL_IP]:3000;
}

server {

    listen       443 ssl http2;
    listen       [::]:443 ssl http2;
    server_name api.mutuiamo.it;

    location / {
        proxy_pass       http://node_app;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_http_version 1.1;
        proxy_set_header   Upgrade $http_upgrade;
        proxy_set_header   Connection "upgrade";
        proxy_set_header   Host $host;
    }

    location /socket.io/ {
        proxy_pass     http://websocket_server;
        proxy_redirect off;
        proxy_http_version 1.1;
        proxy_set_header   Upgrade $http_upgrade;
        proxy_set_header   Connection "upgrade";
        proxy_set_header   Host $host;
        proxy_set_header   X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

}

server {
if ($host = api.mutuiamo.it) {
        return 301 https://$host$request_uri;
}

    listen 80 default_server;
    server_name api.mutuiamo.it;
    return 404;

}
```

Creare link simbolico del file di configurazione

```
ln -s /etc/nginx/sites-available/api.mutuiamo.it.conf /etc/nginx/sites-enabled/api.mutuiamo.it.conf
```

## File di configurazione per advisorpanel.mutuiamo.it

Creare il file di configurazione per l'advisor panel.

```
vi /etc/nginx/sites-available/advisorpanel.mutuiamo.it.conf
```

deve contenere il seguente contenuto, sostituendo [INTERNAL_IP] con il valore trovato prima.

```
server {

    listen       443 ssl http2;
    listen       [::]:443 ssl http2;

    server_name advisorpanel.mutuiamo.it;

    # cartella del dist di angular
    root /usr/share/nginx/advisor_panel/dist/;
    index index.html;

    location / {
      try_files $uri $uri/ =404;
    }

}

server {

    if ($host = panel.mutuiamo.it) {
        return 301 https://$host$request_uri;
    }

    listen 80;
    server_name advisorpanel.mutuiamo.it;
    return 404;

}
```

Creare link simbolico del file di configurazione

```
ln -s /etc/nginx/sites-available/advisorpanel.mutuiamo.it.conf /etc/nginx/sites-enabled/advisorpanel.mutuiamo.it.conf
```

# SSL

## Installazione certificato

```
yum install certbot-nginx
certbot --nginx
```

Seguire le istruzioni per creare il certificato per i due host.

Riavviare NGINX

```
systemctl restart nginx
```

# PM2

## Installazione

```
cd /usr/share/nginx/mobile_sevices
pm2 start src/server.js --name mobile_services
pm2 list
pm2 startup systemd
pm2 save
```

## Comandi utili

```
pm2 info mobile_services
```

Pulire file di log

```
pm2 flush mobile_services
```

Stop, restart, monitor

```
pm2 stop mobile_services
pm2 restart mobile_services
pm2 monit
```

# MONGODB

## Installazione

Caricare repo su yum

```
vi /etc/yum.repos.d/mongodb-org-5.0.repo
```

Aggiungere questo contenuto

```
[mongodb-org-5.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/5.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-5.0.asc
```

```
yum install mongodb-org
mongod --version
```

Se tutto ok avviarli e metterli in start al boot

```
systemctl start mongod
systemctl enable mongod
systemctl status mongod
```

## Abilitazione autenticazione

Aggiungere autenticazione e aggiungere al file

```
vi /etc/mongod.conf
```

```
security:
authorization: enabled
```

## Connessione

Connetersi alla shell di mongo

```
mongo
```

Aggiungere utente dedicato

```
use admin;
db.createUser( { user: "mutuiamo", pwd: passwordPrompt(), roles: [ { role: "dbOwner", db: "mutuiamo" } ]} );
```

Riavvio Mongo

```
systemctl restart mongod.service
```

## Importare collezioni MONGODB

```
mongoimport -h localhost:27017 -d mutuiamo -c users -u mutuiamo -p mutuiamo --drop --file users.json

mongoimport -h localhost:27017 -d mutuiamo -c questions -u mutuiamo -p mutuiamo --drop --file questions.json

mongoimport -h localhost:27017 -d mutuiamo -c terms -u mutuiamo -p mutuiamo --drop --file terms.json

mongoimport -h localhost:27017 -d mutuiamo -c topics -u mutuiamo -p mutuiamo --drop --file topics.json

mongoimport -h localhost:27017 -d mutuiamo -c posts -u mutuiamo -p mutuiamo --drop --file posts.json
```

Connettersi con

```
mongosh --host 127.0.0.1 --port 27017 -u mutuiamo --authenticationDatabase mutuiamo
```

# MYSQL

## Installazione

```
rpm -Uvh https://repo.mysql.com/mysql80-community-release-el7-3.noarch.rpm
sed -i 's/enabled=1/enabled=0/' /etc/yum.repos.d/mysql-community.repo
yum --enablerepo=mysql80-community install mysql-community-server

service mysqld start
```

Segnarsi la password temporanea

```
grep "A temporary password" /var/log/mysqld.log

##### A temporary password is generated for root@localhost: uVz0cfj#cjq&
```

Avvio servizio MYSQL e start al boot

```
service mysqld restart
chkconfig mysqld on
```

## Connessione

Connessione MYSQL

```
mysql -u root -p
CREATE USER 'mutuiamo'@'localhost' IDENTIFIED BY 'XXXXXXXXXXX';
GRANT SELECT, SHOW VIEW ON mutuiamo._ TO 'mutuiamo'@'localhost';
GRANT FILE ON _.\* TO 'mutuiamo'@'localhost';

CREATE DATABASE mutuiamo;
mysql -u root -p mutuiamo < dump.sql
```

# Utenza per CI/CD

Crea utente e aggiungi ai sudoers

```
adduser deploy
passwd deploy
gpasswd -a deploy wheel
```
