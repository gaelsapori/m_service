import express from "express";

// routes
import authRoutes from "../services/auth/auth.route.js";
import otpRoutes from "../services/otp/otp.route.js";
import userRoutes from "../services/user/user.route.js";
import chatRoomRoutes from "../services/room/room.route.js";
import postRoutes from "../services/post/post.route.js";
import fileRoutes from "../services/file/file.route.js";
import agendaRoutes from "../services/agenda/agenda.route.js";
import leadRoutes from "../services/crm/lead.route.js";
import rateRoutes from "../services/rate/rate.route.js";
import ratingRoutes from "../services/rating/rating.route.js";
import prospectRoutes from "../services/prospect/prospect.route.js";
import questionRoutes from "../services/question/question.route.js";
import deleteRoutes from "../services/delete/delete.route.js";
import firebaseRoutes from "../services/firebase/firebase.route.js";

// middlewares
import { decode } from "../middlewares/jwt.js";
import { authLimiter } from "../middlewares/rate_limiter.js";

const router = express.Router();

router.get("/status", (req, res) => res.json({ status: "ok" }));
router.use("/auth/", authLimiter, authRoutes);
router.use("/otp", otpRoutes);
router.use("/users", decode, userRoutes);
router.use("/rooms", decode, chatRoomRoutes);
router.use("/posts", decode, postRoutes);
router.use("/files", decode, fileRoutes);
router.use("/agenda", decode, agendaRoutes);
router.use("/rates", decode, rateRoutes);
router.use("/ratings", decode, ratingRoutes);
router.use("/leads", decode, leadRoutes);
router.use("/questions", decode, questionRoutes);
router.use("/prospects", decode, prospectRoutes);
router.use("/firebase", decode, firebaseRoutes);
router.use("/delete", decode, deleteRoutes);

export default router;
