import dotenv from "dotenv";
dotenv.config();

export default {
  otpSecret: process.env.OTP_SECRET_KEY,
  jwtSecret: process.env.JWT_AUTH_TOKEN,
  xApiKey: process.env.X_API_KEY,
  hashSecret: process.env.HASH_SECRET,
  jwtRefresh: process.env.JWT_REFRESH_TOKEN,
  twillio: {
    sid: process.env.TWILLIO_ACCOUNT_SID,
    token: process.env.TWILLIO_AUTH_TOKEN,
    number: process.env.TWILLIO_NUMBER
  },
  fcmToken: process.env.FCM_SERVER_TOKEN,
  sendGridToken: process.env.SENDGRID_API_KEY,
  fromEmail: process.env.FROM_EMAIL,
  endpoint: {
    rates: process.env.RATES_ENDPOINT,
    lead: process.env.LEAD_ENDPOINT
  },
  clientUrl: process.env.CLIENT_URL,
  redis: {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    passowrd: process.env.REDIS_PASSWORD
  },
  mongo: {
    uri:
      "mongodb://" +
      process.env.MONGO_DB_USERNAME +
      ":" +
      process.env.MONGO_DB_PASSWORD +
      "@" +
      process.env.MONGO_DB_HOST +
      ":" +
      process.env.MONGO_DB_PORT +
      "/" +
      process.env.MONGO_DB_DATABASE,
    db: process.env.MONGO_DB_DATABASE,
    username: process.env.MONGO_DB_USERNAME,
    password: process.env.MONGO_DB_PASSWORD,
    host: process.env.MONGO_DB_HOST,
    port: process.env.MONGO_DB_PORT
  },
  mysql: {
    database: process.env.MYSQL_DATABASE,
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT
  },
  port: process.env.NODE_PORT,
  env: process.env.NODE_ENV,
  rateLimit: {
    seconds: process.env.IP_REQUEST_EXPIRATION,
    otp: process.env.IP_REQUEST_OTP_MAX,
    auth: process.env.IP_REQUEST_AUTH_MAX
  },
  features: {
    sms: process.env.SEND_SMS == "ON",
    email: process.env.SEND_OTP_EMAIL == "ON"
  },
  proxy: process.env.TRUST_PROXY === "ON"
};
