import mongoose from "mongoose";
import logger from "../middlewares/logger.js";
import envar from "./vars.js";

mongoose.connection.on("connected", () => {
  logger.debug("[MongoDB] connected succesfully");
});

mongoose.connection.on("error", (error) => {
  console.error("[MongoDB] connection has an error");
  mongoose.disconnect();
});

export default {
  connect: () => {
    mongoose.connect(envar.mongo.uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    return mongoose.connection;
  }
};
