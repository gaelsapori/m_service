import mysql from "mysql2";
import envar from "../config/vars.js";

let dbConfig = {
  connectionLimit: 10,
  host: envar.mysql.host,
  user: envar.mysql.user,
  password: envar.mysql.password,
  database: envar.mysql.database,
  port: envar.mysql.port,
  timezone: "+00:00"
};
const pool = mysql.createPool(dbConfig);

export default {
  connection: () =>
    new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) reject(err);

        const query = (sql, binding) => {
          return new Promise((resolve, reject) => {
            connection.query(sql, binding, (err, result) => {
              if (err) reject(err);
              resolve(result);
            });
          });
        };
        const release = () => {
          return new Promise((resolve, reject) => {
            if (err) reject(err);
            resolve(connection.release());
          });
        };
        resolve({ query, release });
      });
    }),
  query: (sql, binding) =>
    new Promise((resolve, reject) => {
      pool.query(sql, binding, (err, result, fields) => {
        if (err) reject(err);
        resolve(result);
      });
    })
};
