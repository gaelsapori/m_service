import envar from "./vars.js";
import express from "express";
import morgan from "morgan";
import cors from "cors";
import helmet from "helmet";
import v1 from "../routes/v1.js";
import {
  converter,
  notFound,
  handler as errorHandler
} from "../middlewares/error.js";

const allowlist = [
  "https://advisor-panel.mutuiamolabs.com/",
  `https://advisor-panel.mutuiamo.it`,
  "http://localhost:4200",
  `https://${envar.clientUrl}`
];

const app = express();
if (envar.env === "development") app.use(morgan("tiny"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(helmet());
app.use(cors({ origin: allowlist, optionsSuccessStatus: 200 }));

app.use("/v1", v1);
app.set("port", envar.port);
app.set("trust proxy", envar.proxy);

app.use(converter);
app.use(notFound);
app.use(errorHandler);

export default app;
