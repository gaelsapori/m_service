import axios from "axios";
import envar from "../../config/vars.js";

export default {
  create: async (body) => {
    return axios.post(envar.endpoint.lead, body);
  }
};
