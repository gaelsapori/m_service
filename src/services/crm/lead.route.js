import express from "express";
import { validate } from "express-validation";
import controller from "./lead.controller.js";
import validation from "./lead.validation.js";
const router = express.Router();

/**
 * @api {post} /v1/leads                    create lead
 * @apiName                                 CreateLeads
 * @apiDescription                          Create a lead on CRM.
 * @apiGroup                                Leads
 * @apiVersion                              0.1.0
 * @apiPermission                           authenticated
 * @apiUse authenticated
 *
 */

router.post("/", validate(validation.lead), controller.onCreate);

export default router;
