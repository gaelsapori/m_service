import Joi from "joi";

export default {
  lead: {
    body: Joi.object({
      values: Joi.string().required()
    })
  }
};
