import mysql from "../../config/mysql.js";

export default {
  exportedPhone: async (phone) => {
    const connection = await mysql.connection();
    try {
      let rows = await connection.query(
        "SELECT `lead`.* FROM `lead_esportata`" +
          "JOIN `lead` ON `lead_esportata`.`lead_id` = `lead`.`id`" +
          "WHERE `lead`.`telefono` = ? " +
          "ORDER BY `lead_esportata`.`id` DESC LIMIT 1",
        [phone]
      );
      return rows[0];
    } finally {
      await connection.release();
    }
  },
  lead: async (leadId) => {
    const connection = await mysql.connection();
    try {
      let rows = await connection.query(
        "SELECT * FROM `lead` WHERE `lead`.`id` = ? ",
        [leadId]
      );
      return rows[0];
    } finally {
      await connection.release();
    }
  },
  survey: async (field, value) => {
    const connection = await mysql.connection();
    try {
      let rows = await connection.query(
        "SELECT " +
          "`assegnato_ad_agente_di_zona`.`agente_di_zona_id` as advisor_id ," +
          "`lead`.`id` as lead_id," +
          // dati personali
          "`lead`.`nome`," +
          "`lead`.`cognome`," +
          "`lead`.`data_nascita` as birthDate1," +
          "`lead`.`sesso`," +
          "`lead`.`provincia`," +
          "`lead`.`cf`," +
          "`lead`.`comune`," +
          "`lead`.`email`," +
          "`lead`.`telefono`," +
          "(CASE " +
          "  WHEN `questionario_lead`.`immobili_visitati` = 'compromise' THEN '0' " +
          "  WHEN `questionario_lead`.`immobili_visitati` = 'now' THEN '5' " +
          "  WHEN `questionario_lead`.`immobili_visitati` = 'done' THEN '100' " +
          "  WHEN `questionario_lead`.`immobili_visitati` = 'gt_5' THEN '1' " +
          "  ELSE null " +
          "END) as research_stage, " +
          // dati nucleo
          "`lead`.`nucleo_familiare`," +
          "`lead`.`numero_persone_con_reddito`," +
          // dati primo intestatario
          "`lead`.`posizione_lavorativa`, " +
          "`lead`.`numero_mensilita` as salaryNumber1," +
          "`lead`.`reddito_mensile_netto` as income1," +
          "`lead`.`ha_finanziamento` as hasLoanCost1," +
          "`lead`.`spesa_mensile_finanziamento` as loanCost1," +
          // dati secondo intestatario
          "`intestatari_questionario_lead`.`nome` as nome2, " +
          "`intestatari_questionario_lead`.`cognome` as cognome2, " +
          "`intestatari_questionario_lead`.`posizione_lavorativa` as posizione_lavorativa2, " +
          "`intestatari_questionario_lead`.`numero_mensilita` as salaryNumber2, " +
          "`intestatari_questionario_lead`.`reddito_mensile_netto` as income2, " +
          "`intestatari_questionario_lead`.`ha_finanziamento` as hasLoanCost2, " +
          "`intestatari_questionario_lead`.`spesa_mensile_finanziamento` as loanCost2, " +
          "`intestatari_questionario_lead`.`data_nascita` as birthDate2," +
          // dati mutuo
          "`questionario_lead`.`prezzo_immobile`," +
          "`questionario_lead`.`valore_mutuo`," +
          "IF(`questionario_lead`.`durata_mutuo` = 0, 30, durata_mutuo) as durata_mutuo," +
          "`questionario_lead`.`numero_intestatari`," +
          "`questionario_lead`.`rata_max_mutuo`," +
          "`questionario_lead`.`periodo_acquisizione`, " +
          "`questionario_lead`.`tipo_immobile`, " +
          "`questionario_lead`.`comune_immobile` ," +
          "`questionario_lead`.`compilato_il` " +
          "FROM `assegnato_ad_agente_di_zona`" +
          "JOIN `lead` ON `assegnato_ad_agente_di_zona`.`lead_id` = `lead`.`id` " +
          "JOIN `questionario_lead` ON `questionario_lead`.`lead_id` = `lead`.`id` " +
          "LEFT JOIN `intestatari_questionario_lead` ON `intestatari_questionario_lead`.`questionario_lead_id` = `lead`.`id` " +
          "WHERE `lead`.`" +
          field +
          "` = ? " +
          "ORDER BY `questionario_lead`.`compilato_il` DESC  LIMIT 1",
        [value]
      );
      return rows[0];
    } finally {
      await connection.release();
    }
  },
  leadAdvisor: async (leadId) => {
    const connection = await mysql.connection();
    try {
      let rows = await connection.query(
        "SELECT " +
          "`agente`.`id`, " +
          "`agente`.`nome`, " +
          "`agente`.`cognome`, " +
          "`agente`.`email`, " +
          "`agente`.`telefono`, " +
          "(" +
          " SELECT COUNT(*) from assegnato_ad_agente_di_zona " +
          " JOIN `lead_esportata` ON `lead_esportata`.`lead_id` = `assegnato_ad_agente_di_zona`.`lead_id`" +
          " WHERE assegnato_ad_agente_di_zona.agente_di_zona_id = `agente`.`id` " +
          ") as supportedUsers " +
          "FROM `assegnato_ad_agente_di_zona`" +
          "JOIN `agente` ON `assegnato_ad_agente_di_zona`.`agente_di_zona_id` = `agente`.`id`" +
          "WHERE `assegnato_ad_agente_di_zona`.`lead_id` = ? " +
          "ORDER BY `assegnato_ad_agente_di_zona`.`id` LIMIT 1",
        [leadId]
      );
      return rows[0];
    } finally {
      await connection.release();
    }
  },
  advisor: async (advisorId) => {
    const connection = await mysql.connection();
    try {
      let rows = await connection.query(
        "SELECT " +
          "`agente`.`id`, " +
          "`agente`.`nome`, " +
          "`agente`.`cognome`, " +
          "`agente`.`email`, " +
          "`agente`.`telefono`, " +
          "(" +
          " SELECT COUNT(*) from assegnato_ad_agente_di_zona " +
          " JOIN `lead_esportata` ON `lead_esportata`.`lead_id` = `assegnato_ad_agente_di_zona`.`lead_id`" +
          " WHERE assegnato_ad_agente_di_zona.agente_di_zona_id = `agente`.`id` " +
          ") as supportedUsers " +
          "FROM `agente` WHERE `agente`.`id` = ? ",
        [advisorId]
      );
      return rows[0];
    } finally {
      await connection.release();
    }
  },
  findMostRecentExported: async (phoneNumber) => {
    const connection = await mysql.connection();
    try {
      let rows = await connection.query(
        "SELECT `lead_esportata`.id, user.id as advisorId ,`lead`.id as leadId FROM `lead_esportata` " +
          "JOIN `lead` ON `lead_esportata`.`lead_id` = `lead`.`id` " +
          "JOIN assegnato_ad_agente_di_zona aadz ON aadz.lead_id = `lead_esportata`.`lead_id` " +
          "JOIN `user` ON `user`.`id` = aadz.agente_di_zona_id  " +
          "WHERE `lead`.`telefono` = ? AND `user`.`enabled` = 1 " +
          "ORDER BY `lead_esportata`.`id` DESC ",
        [phoneNumber]
      );
      return rows;
    } finally {
      await connection.release();
    }
  },
  findLeadExported: async (advisorId, appUserIds) => {
    const connection = await mysql.connection();
    try {
      let rows = await connection.query(
        "SELECT `lead_esportata`.`lead_id` as leadId from assegnato_ad_agente_di_zona " +
          "JOIN `lead_esportata` ON `lead_esportata`.`lead_id` = `assegnato_ad_agente_di_zona`.`lead_id` " +
          "WHERE assegnato_ad_agente_di_zona.agente_di_zona_id = ? " +
          "AND `lead_esportata`.`lead_id` IN ? ",
        [advisorId, [appUserIds]]
      );
      return rows;
    } finally {
      await connection.release();
    }
  },
  myProspects: async (leadId, withRdv) => {
    const connection = await mysql.connection();
    try {
      const withRdvQuery =
        withRdv == true
          ? " WHERE pm.data_appuntamento IS NOT NULL AND pm.data_appuntamento >= CURDATE()"
          : "";
      const order =
        withRdv == true
          ? " ORDER BY pm.data_appuntamento ASC"
          : " ORDER BY pm.created_at DESC";
      const query =
        "SELECT " +
        "pm.id as prospect_id, " +
        "pm.id, " +
        "pm.lead_id, " +
        "pm.tipologia_mutuo, " +
        "pm.durata_mutuo, " +
        "pm.tan, " +
        "pm.house_prospect_price as prezzo_immobile, " +
        "pm.mortgage_prospect_value as valore_mutuo, " +
        "pm.prodotto_id, " +
        "pm.taeg, " +
        "pm.pagamento_mensile, " +
        "pm.data_appuntamento, " +
        "pm.extra_spese, " +
        "pm.created_at," +
        "b.id as banca_id," +
        "b.codice_banca," +
        "p.codice_prodotto, " +
        "b.codice_ABI, " +
        "b.nome_banca, " +
        "p.nome as nome_prodotto," +
        "p.tax_type," +
        "p.id," +
        "(CASE " +
        "  WHEN p.tax_type = 'fisso' THEN 'Fisso' " +
        "  WHEN p.tax_type = 'variabile_cap' THEN 'Variabile con Cap' " +
        "  WHEN p.tax_type = 'variabile' THEN 'Variabile' " +
        "  ELSE 'Fisso' " +
        "END) as tipo_tasso, " +
        "f.codice_cab as cab," +
        "f.indirizzo, " +
        "f.comune, " +
        "f.provincia, " +
        "f.telefono AS telefono_filiale," +
        "l.nome, " +
        "l.cognome, " +
        "l.telefono, " +
        "l.email, " +
        "l.cf, " +
        "l.reddito_mensile_netto, " +
        "l.posizione_lavorativa, " +
        "ql.tipo_immobile, " +
        "ql.numero_intestatari " +
        "FROM prospetti_mutuo pm " +
        "INNER JOIN ( " +
        "SELECT p.banca_id, max(pm.id) as id " +
        "FROM prospetti_mutuo pm, prodotti p " +
        "WHERE lead_id = ? AND p.id = pm.prodotto_id " +
        "AND pm.created_at > '2022-03-02' " +
        "GROUP BY p.banca_id " +
        ") s  ON pm.id = s.id " +
        "LEFT JOIN prodotti p ON p.id = pm.prodotto_id " +
        "LEFT JOIN banca b ON b.id = p.banca_id " +
        "LEFT JOIN `lead` l ON l.id = pm.lead_id " +
        "LEFT JOIN filiali f ON f.codice_cab = pm.cab AND f.banca_id = b.id " +
        "LEFT JOIN questionario_lead ql ON ql.lead_id = pm.lead_id " +
        withRdvQuery +
        order;
      return await connection.query(query, [leadId]);
    } finally {
      await connection.release();
    }
  },
  transform(leadData) {
    return 3;
  }
};

/**
 * @api {OBJECT} Prospect Prospect
 * @apiGroup Models
 * @apiVersion 0.1.0
 * @apiParam  {String}        id                                    Prospect ID.
 * @apiParam  {String}        lead_id                               Lesd ID.
 * @apiParam  {String}        cab                                   Bank branch CAB.
 * @apiParam  {String}        tipologia_mutuo                       Mortgage type.
 * @apiParam  {String}        durata_mutuo                          Mortgage duration.
 * @apiParam  {String}        tan                                   Mortgage interest.
 * @apiParam  {String}        taeg                                  Mortgage full cost.
 * @apiParam  {String}        pagamento_mensile                     Monthly fee.
 * @apiParam  {String}        data_appuntamento                     Branch appointment date.
 * @apiParam  {String}        extra_spese.impostaSostitutiva        Other taxes.
 * @apiParam  {String}        extra_spese.periodicCost              Periodic costs.
 * @apiParam  {String}        extra_spese.speseIstruttoria          Mortgage operation expenses.
 * @apiParam  {String}        extra_spese.spesePerizia              Mortgage costs expertise.
 * @apiParam  {String}        extra_spese.polizzaAntincendio        Fire insurance policy.
 * @apiParam  {String}        created_at                            Prospect creation date.
 * @apiParam  {String}        prodotto_id                           Product Id.
 * @apiParam  {String}        nome_prodotto                         Product name.
 * @apiParam  {String}        nome                                  Customer name.
 * @apiParam  {String}        cognome                               Customer lastname.
 * @apiParam  {String}        telefono                              Customer phone.
 * @apiParam  {String}        email                                 Customer email.
 * @apiParam  {String}        cf                                    Customer tax code.
 * @apiParam  {String}        reddito_mensile_netto                 Customer monthly income.
 * @apiParam  {String}        posizione_lavorativa                  Customer job title.
 * @apiParam  {String}        prezzo_immobile                       House price.
 * @apiParam  {String}        valore_mutuo                          Mortgage value.
 * @apiParam  {String}        tipo_immobile                         House type.
 * @apiParam  {String}        numero_intestatari                    Mortgage owners number.
 * @apiParam  {String}        codice_prodotto                       Product code.
 * @apiParam  {String}        banca_id                              Bank id.
 * @apiParam  {String}        codice_banca                          Bank code.
 * @apiParam  {String}        codice_ABI                            Branch code.
 * @apiParam  {String}        nome_banca                            Bank name.
 * @apiParam  {String}        indirizzo                             Branch address.
 * @apiParam  {String}        comune                                Branch city.
 * @apiParam  {String}        provincia                             Branch province.
 * @apiParam  {String}        telefono_filiale                      BRanch phone number.
 */
