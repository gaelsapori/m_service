import User from "../user/user.model.js";
import hashUtil from "../../utils/hash.js";
import httpStatus from "http-status";
import APIError from "../../utils/api.error.js";
import { handler as errorHandler } from "../../middlewares/error.js";
import service from "./lead.service.js";

export default {
  onCreate: async (req, res) => {
    try {
      const values = JSON.parse(req.body.values);
      const user = await User.get(req.userId);
      const dataNascita = values["borrowers"][0]["borrower_birth_date"];
      const age = hashUtil.computeAge(dataNascita);

      user.type = User.types.BUDGET;
      user.cityId = values["house_city"]["idComune"];
      user.birthDate = dataNascita;
      user.city = values["house_city"];
      user.researchStage = values["house_visited"];
      user.age = age;
      user.exported = false;
      User.updateUser(user);

      const ratesRequest = await service.create({
        version: "1.2.7",
        values: values,
        user: {},
        referral_ad: "mutuiamo_app",
        tracking: {
          utm_source: "mutuiamo",
          utm_medium: "app",
          utm_campaign: "survey"
        }
      });
      if (ratesRequest.data.statusCode != httpStatus.OK) {
        throw new APIError({
          message: "Qualcosa è andato storto",
          status: httpStatus.INTERNAL_SERVER_ERROR
        });
      }

      return res.json({ success: true, user });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
