import express from "express";
import deleteController from "./delete.controller.js";
import fileController from "../file/file.controller.js";
import userController from "../user/user.controller.js";
import postController from "../post/post.controller.js";
import role from "../../middlewares/role.js";

const router = express.Router();

/**
 * @api {delete} /v1/delete/room/:roomId         delete room
 * @apiName                                      DeleteRoom
 * @apiDescription                               Delete one room and its messages.
 * @apiGroup                                     Rooms
 * @apiVersion                                   0.1.0
 * @apiPermission                                admin
 * @apiUse admin
 *
 * @apiParam    {String}        roomId                Room ID.
 *
 * @apiSuccess  {Boolean}       success               Authorized?
 * @apiSuccess  {String}        message               Operation result.
 * @apiSuccess  {String}        deletedRoomsCount     Count of room deleted.
 * @apiSuccess  {String}        deletedMessagesCount  Count of messages deleted.
 *
 */
router.delete("/room/:roomId", role.isAdmin, deleteController.onDeleteRoom);

/**
 * @api {delete} /v1/delete/message/:messageId    delete message
 * @apiName                                       DeleteMessage
 * @apiDescription                                Delete one message.
 * @apiGroup                                      Rooms
 * @apiVersion                                    0.1.0
 * @apiPermission                                 admin
 * @apiUse admin
 *
 * @apiParam    {String}        messageId             Message ID.
 *
 * @apiSuccess  {Boolean}       success               Authorized?
 * @apiSuccess  {String}        message               Operation result.
 * @apiSuccess  {String}        deletedMessagesCount  Count of messages deleted.
 *
 */
router.delete(
  "/message/:messageId",
  role.isAdmin,
  deleteController.onDeleteMessage
);

/**
 * @api {delete} /v1/delete/user/:id            delete user
 * @apiName                                     DeleteUser
 * @apiDescription                              Delete one user.
 * @apiGroup                                    Users
 * @apiVersion                                  0.1.0
 * @apiPermission                               admin
 * @apiUse admin
 *
 * @apiParam    {String}        id          User ID.
 *
 * @apiSuccess  {Boolean}       success     Authorized?
 * @apiSuccess  {String}        message     Operation result.
 *
 */
router.delete("/user/:id", role.isAdmin, userController.onDelete);

/**
 * @api {delete} /v1/delete/image/:fileName         delete image
 * @apiName                                         DeleteImage
 * @apiDescription                                  Delete one image.
 *
 * Here you can delete a post image or a profile image. their names start with mutuiamo_ and belongs to MongoDB bucket named "images".
 * @apiGroup                                        Posts
 * @apiVersion                                      0.1.0
 * @apiPermission                                   admin and editor
 * @apiUse admin
 *
 * @apiParam    {String}        fileName      File name.
 *
 * @apiSuccess  {Boolean}       success       Authorized?
 * @apiSuccess  {String}        message       Operation result.
 *
 */
router.delete("/image/:filename", role.isEditor, fileController.onDeleteImage);

/**
 * @api {delete} /v1/delete/document/:fileName          delete document
 * @apiName                                             DeleteDocument
 * @apiDescription                                      Delete one document, it represents a chat message file.
 *
 * Here you can delete a document. their names start with document_ and belongs to MongoDB bucket named "documents".
 * @apiGroup                                            Rooms
 * @apiVersion                                          0.1.0
 * @apiPermission                                       admin
 * @apiUse admin
 *
 * @apiParam    {String}        fileName    File name.
 *
 * @apiSuccess  {Boolean}       success     Authorized?
 * @apiSuccess  {String}        message     Operation result.
 *
 */
router.delete(
  "/document/:filename",
  role.isAdmin,
  fileController.onDeleteDocument
);

/**
 * @api {delete} /v1/delete/post/:postId                delete post
 * @apiName                                             DeleteUser
 * @apiDescription                                      Delete one post.
 * @apiGroup                                            Users
 * @apiVersion                                          0.1.0
 * @apiPermission                                       admin
 * @apiUse admin
 *
 * @apiParam    {String}        postId      Post ID.
 *
 * @apiSuccess  {Boolean}       success     Authorized?
 * @apiSuccess  {String}        message     Operation result.
 *
 */
router.delete("/post/:id", role.isEditor, postController.onDelete);

/**
 * @api {delete} /v1/delete/document/:fileName          delete document
 * @apiName                                             DeleteDocument
 * @apiDescription                                      Delete one document, it represents a chat message file.
 *
 * Here you can delete a document. their names start with document_ and belongs to MongoDB bucket named "documents".
 * @apiGroup                                            Rooms
 * @apiVersion                                          0.1.0
 * @apiPermission                                       admin
 * @apiUse admin
 *
 * @apiParam    {String}        fileName    File name.
 *
 * @apiSuccess  {Boolean}       success     Authorized?
 * @apiSuccess  {String}        message     Operation result.
 *
 */
router.delete(
  "/rating/:ratingId",
  role.isAdmin,
  deleteController.onDeleteRating
);

export default router;
