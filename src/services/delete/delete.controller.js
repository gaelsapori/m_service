import Room from "../room/room.model.js";
import Message from "../room/message.model.js";
import Rating from "../rating/rating.model.js";
import { handler as errorHandler } from "../../middlewares/error.js";

export default {
  onDeleteRoom: async (req, res) => {
    try {
      const room = await Room.deleteOne({ _id: req.params.roomId });
      const messages = await Message.deleteMany({ roomId: req.params.roomId });
      return res.json({
        success: true,
        deletedRooms: room.deletedCount,
        deletedMessages: messages.deletedCount
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onDeleteMessage: async (req, res) => {
    try {
      const message = await Message.deleteOne({ _id: req.params.messageId });
      return res.json({
        success: true,
        deletedMessages: message.deletedCount
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onDeleteRating: async (req, res) => {
    try {
      const rating = await Rating.deleteOne({ _id: req.params.ratingId });
      return res.json({
        success: true,
        deletedRating: rating.deletedCount
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
