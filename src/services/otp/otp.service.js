import twilio from "twilio";
import httpStatus from "http-status";
import envar from "../../config/vars.js";
import CrmLookup from "../crm/crm.lookup.js";
import mailer from "../../utils/mailer.js";
import APIError from "../../utils/api.error.js";
import hashUtil from "../../utils/hash.js";
import logger from "../../middlewares/logger.js";

const client = twilio(envar.twillio.sid, envar.twillio.token);

export default {
  buildOtp: async (phone) => {
    const exportedUser = await CrmLookup.exportedPhone(phone);
    let otp = Math.floor(1000 + Math.random() * 9000);
    if (phone == "321321321") otp = 3210;
    const fullHash = hashUtil.generate(phone, otp);
    const payload = {
      exported: exportedUser != null,
      phone,
      hash: fullHash,
      otp
    };
    return payload;
  },
  verifiyOtp: ({ phone, hash, otp }) => {
    const [hashValue, expires] = hash.split(".");
    if (Date.now() > parseInt(expires)) {
      throw new APIError({
        message: "Codice scaduto.",
        status: httpStatus.OK
      });
    }
    const valid = hashUtil.otp(`${phone}.${otp}.${expires}`) === hashValue;
    if (!valid) {
      throw new APIError({
        message: "Il codice non è corretto.",
        status: httpStatus.OK
      });
    }
    return valid;
  },
  mail: async (email, otp) => {
    await mailer.sendEmailOtp(email, otp);
  },
  sms: async (phone, otp) => {
    const sms = `Ecco il tuo codice di sicurezza Mutuiamo: ${otp}. Scade tra 5 minuti.`;
    client.messages
      .create({ body: sms, from: envar.twillio.number, to: `+39${phone}` })
      .then((messages) => {
        logger.info(`[Twilio] SMS sent to ${phone} ${messages.body}`);
      })
      .catch((err) => logger.error(err));
  }
};
