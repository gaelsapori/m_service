import Joi from "joi";

export default {
  // POST /v1/otp/checkphone
  phone: {
    body: Joi.object({ phone: Joi.string().required().min(8).max(10) })
  },
  otp: {
    body: Joi.object({
      phone: Joi.string().required().min(8).max(10),
      hash: Joi.string().required(),
      otp: Joi.number().required(),
      exported: Joi.boolean()
    })
  }
};
