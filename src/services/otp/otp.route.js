import express from "express";
import { validate } from "express-validation";
import controller from "./otp.controller.js";
import validation from "./otp.validation.js";
import {
  phoneOtpLimiter,
  emailOtpLimiter,
  verifyOtpLimiter,
  checkPhoneLimiter
} from "../../middlewares/rate_limiter.js";
const router = express.Router();

/**
 * @api {post} /v1/otp/checkphone                check phone
 * @apiName                                      CheckPhone
 * @apiDescription                               Check phone number existence on CRM.
 * @apiGroup                                     OTP
 * @apiVersion                                   0.1.0
 * @apiPermission                                none
 *
 * @apiParam    {String}    phone           Customer phone number.
 *
 * @apiSuccess  {Boolean}   success         Phone number exists?
 *
 */
router.post(
  "/checkphone",
  [checkPhoneLimiter, validate(validation.phone)],
  controller.onCheckPhone
);

/**
 * @api {post} /v1/otp/phone                     send otp [phone]
 * @apiName                                      PhoneOTP
 * @apiDescription                               Send OTP to phone number if exists.
 * @apiGroup                                     OTP
 * @apiVersion                                   0.1.0
 * @apiPermission                                none
 *
 * @apiParam    {String}    phone         Customer phone number.
 *
 * @apiSuccess  {Boolean}   success       Phone number exists?
 * @apiSuccess  {String}    phone         Phone number.
 * @apiSuccess  {String}    hash          OTP hash.
 * @apiSuccess  {Number}    otp           OTP code.
 *
 */
router.post(
  "/phone",
  [phoneOtpLimiter, validate(validation.phone)],
  controller.onPhone
);

/**
 * @api {post} /v1/otp/email                     send otp [email]
 * @apiName                                      EmailOTP
 * @apiDescription                               Send OTP to email if exists.
 * @apiGroup                                     OTP
 * @apiVersion                                   0.1.0
 * @apiPermission                                none
 *
 * @apiParam    {String}    phone         Customer phone.
 *
 * @apiSuccess  {Boolean}   success       Phone number exists?
 * @apiSuccess  {String}    phone         Phone number.
 * @apiSuccess  {String}    hash          OTP hash.
 * @apiSuccess  {Number}    otp           OTP code.
 *
 */
router.post(
  "/email",
  [emailOtpLimiter, validate(validation.phone)],
  controller.onEmail
);

/**
 * @api {post} /v1/otp/verify                           verify otp
 * @apiName                                             VerifyOTP
 * @apiDescription                                      Verify OTP.
 * @apiGroup                                            OTP
 * @apiVersion                                          0.1.0
 * @apiPermission                                       none
 *
 * @apiParam    {String}    otp           OTP.
 * @apiParam    {String}    hash          OTP hash.
 * @apiParam    {String}    phone         Phone number.
 *
 * @apiSuccess  {Boolean}   success         Valid OTP?
 * @apiSuccess  {String}    message             Response message.
 * @apiSuccess  {String}    token           Token.
 *
 */
router.post(
  "/verify",
  [verifyOtpLimiter, validate(validation.otp)],
  controller.onVerify
);

export default router;
