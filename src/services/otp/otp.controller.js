import hashUtil from "../../utils/hash.js";
import envar from "../../config/vars.js";
import service from "./otp.service.js";
import userService from "../user/user.service.js";
import roomService from "../room/room.service.js";
import CrmLookup from "../crm/crm.lookup.js";
import { handler as errorHandler } from "../../middlewares/error.js";

export default {
  onCheckPhone: async (req, res) => {
    try {
      const { phone } = req.body;
      const exportedUser = await CrmLookup.exportedPhone(phone);
      const user = await userService.findOneBy({ phone: phone }, false);
      const verified = user != null && user.otpVerified == true;
      var payload = {
        success: true,
        verified,
        exported: exportedUser != null
      };
      if (verified && user.email)
        payload.email = hashUtil.emailMask(user.email);

      return res.json(payload);
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onPhone: async (req, res) => {
    try {
      const { phone } = req.body;
      const payload = await service.buildOtp(phone);

      if (envar.features.sms) {
        await service.sms(phone, payload.otp);
        delete payload.otp;
      }
      return res.json({ success: true, ...payload });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onEmail: async (req, res) => {
    try {
      const { phone } = req.body;
      const payload = await service.buildOtp(phone);

      if (envar.features.email) {
        const conditions = { phone: phone, type: "customer" };
        const user = await userService.findOneBy(conditions, true);
        await service.mail(user.email, payload.otp);
        delete payload.otp;
      }

      return res.json({ success: true, ...payload });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onVerify: async (req, res) => {
    try {
      let user, advisor;
      const { phone } = req.body;
      service.verifiyOtp(req.body);

      const leadData = await CrmLookup.exportedPhone(phone);
      if (leadData == null) {
        /* CREATE SURVEY USER */
        user = await userService.findOneBy({ phone: phone });
        if (!user) {
          user = await userService.create(
            {
              type: "survey",
              phone: phone,
              otpVerified: true
            },
            false
          );
        }
      } else {
        const agentData = await CrmLookup.leadAdvisor(leadData.id);
        leadData.advisor_id = agentData.id;
        /* CREATE OR UPDATE CUSTOMER USER */
        const customer = userService.transformLead(leadData);
        user = await userService.findOneBy({ phone, leadId: customer.leadId });
        if (!user) {
          user = await userService.create(customer, false);
        } else {
          customer._id = user._id;
          user = await userService.update(customer, false);
        }

        /* CREATE OR UPDATE AGENT ADVISOR */
        const agent = userService.transformAgent(agentData);
        advisor = await userService.findOneAdvisor(agent.advisorId);
        if (!advisor) {
          advisor = await userService.create(agent);
        } else {
          agent._id = advisor._id;
          advisor = await userService.update(agent);
        }

        await roomService.initiate(
          [user._id, advisor._id],
          "advisor-to-customer",
          advisor._id
        );
      }

      return res.json({
        success: true,
        message: "Codice valido",
        type: user.type,
        token: user.token()
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
