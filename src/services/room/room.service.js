import Room from "./room.model.js";
import Message from "./message.model.js";
import httpStatus from "http-status";
import APIError from "../../utils/api.error.js";

export default {
  initiate: async (partecipantsIds, type, chatInitiator) => {
    return await Room.initiateChat(partecipantsIds, type, chatInitiator);
  },
  message: async (roomId, message, currentUser, type) => {
    return await Message.createMsgInRoom(roomId, message, currentUser, type);
  },
  find: async (roomId, currentUser) => {
    const conditions = { _id: roomId };
    if (
      !["admin", "advisor"].includes(currentUser.type) &&
      currentUser.referente != ""
    ) {
      conditions.userIds = { $in: [currentUser.id] };
    }
    const room = await Room.findOne(conditions);
    if (!room)
      throw new APIError({
        message: "Non stai partecipando alla conversazione.",
        status: httpStatus.FORBIDDEN
      });
    return room;
  },
  recipient: async (roomId, userId) => {
    const room = await Room.getRoomRecipient(roomId, userId);
    if (!room)
      throw new APIError({
        message: "Non stai partecipando alla conversazione.",
        status: httpStatus.FORBIDDEN
      });
    return room.recipient;
  },
  conversation: async (roomId, options) => {
    return await Message.getConversationByRoomId(roomId, options);
  },
  recentRooms: async (currentUser) => {
    return await Room.getRecentRooms(currentUser);
  },
  roomUnread: async (currentUser) => {
    return await Room.listUnreads();
  },
  unread: async (roomId, currentLoggedUser) => {
    const msg = await Message.getUnreadRoomMessage(roomId, currentLoggedUser);
    return {
      unread: msg ? msg.unreadMessages : 0,
      total: msg ? msg.messageCount : 0
    };
  },
  markRead: async (roomId, currentLoggedUser) => {
    return await Message.markMessageRead(roomId, currentLoggedUser);
  },
  checkReplace: async (roomId, oldAdvisor) => {
    const room = await Room.findOne({
      _id: roomId,
      userIds: { $in: [oldAdvisor] }
    });
    if (!room)
      throw new APIError({
        message: "La conversazione non esiste.",
        status: httpStatus.NOT_FOUND
      });
    return room;
  },
  replace: async (roomId, userIds) => {
    return await Room.updateRoomPartecipants(roomId, userIds);
  },
  types: Message.types
};
