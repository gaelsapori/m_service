import express from "express";
import { validate } from "express-validation";
import controller from "./room.controller.js";
import validation from "./room.validation.js";
import role from "../../middlewares/role.js";
import { pdf, document } from "../../middlewares/upload.js";
const router = express.Router();

router.param("roomId", controller.roomCheck);

//router.get("/unread", controller.onListUnreadRooms);

/**
 * @api {get} /v1/rooms                 get recent conversations
 * @apiName                             RoomsRecentConversations
 * @apiDescription                      Get list of rooms with last message.
 * @apiGroup                            Rooms
 * @apiVersion                          0.1.0
 * @apiPermission                       authenticated
 * @apiUse authenticated
 *
 * @apiSuccess  {Boolean}                             success                      Authorized?
 * @apiSuccess  {Object[]}                            rooms                        List of rooms.
 * @apiSuccess  {String}                              rooms.roomId                 Room ID.
 * @apiSuccess  {[User](#api-Models-ObjectUser)}      room.recipient               User recipient.
 * @apiSuccess  {Object}                              [room.message]               Last message.
 * @apiSuccess  {String}                              [room.message.messageText]   Last message text.
 * @apiSuccess  {[User](#api-Models-ObjectUser)}      [room.from]                  Last message sender.
 * @apiSuccess  {String="text", "image"}              [room.type]                  Last message type.
 * @apiSuccess  {Object[]}                            [room.readStatus]            List of last message reader.
 * @apiSuccess  {String}                              [room.readStatus.readBy]     Last message Reader ID.
 * @apiSuccess  {String}                              [room.readStatus.readAt]     Last message reading time in ISO string.
 * @apiSuccess  {Number}                              room.unreadMessages          Count of unread messages.
 * @apiSuccess  {String}                              [room.messageSentAt]         Last message creation date in ISO string.
 * @apiSuccess  {String}                              room.chatInitiator           User ID of whom initiate the chat.
 * @apiSuccess  {String}                              room.createdAt               Room creation date in ISO string.
 *
 */
router.get("/", controller.onRecentRooms);

/**
 * @api {get} /v1/rooms/:roomId             get room
 * @apiName                                 GetRoom
 * @apiDescription                          Get one room and its messages.
 * @apiGroup                                Rooms
 * @apiVersion                              0.1.0
 * @apiPermission                           authenticated
 * @apiUse authenticated
 *
 * @apiQuery    {String}                            roomId                              Room ID.
 *
 * @apiSuccess  {Boolean}                           success                             Authorized?
 * @apiSuccess  {Number}                            unread                              Count of unread messages.
 * @apiSuccess  {[Room](#api-Models-ObjectRoom)}    room                                Room data.
 * @apiSuccess  {Object}                            [rating]                            Rating recap.
 * @apiSuccess  {Number}                            [rating.avg]                        Average rating.
 * @apiSuccess  {Number}                            [rating.min]                        Minimum rate recieved.
 * @apiSuccess  {Number}                            [rating.max]                        Maximium rate recieved.
 * @apiSuccess  {Number}                            [rating.count]                      Count of vote recieved.
 * @apiSuccess  {String}                            [rating.advisorId]                  Advisor ID.
 * @apiSuccess  {Boolean}                           [rating.hasVoted]                   Did the user vote already the advisor?.
 * @apiSuccess  {Object[]}                          [conversation]                      List of conversation messages.
 * @apiSuccess  {String}                            [conversation._id]                  Message ID.
 * @apiSuccess  {String}                            [conversation.roomId]               Room ID.
 * @apiSuccess  {Object}                            [conversation.message]              Message object.
 * @apiSuccess  {String}                            [conversation.message.messageText]  Message Text.
 * @apiSuccess  {String}                            [conversation.type]                 Message Type.
 * @apiSuccess  {[User](#api-Models-ObjectUser)}    [conversation.from]                 Message sender.
 * @apiSuccess  {Object[]}                          [conversation.readStatus]           List of message reader.
 * @apiSuccess  {String}                            [conversation.readStatus.readBy]    Message Reader ID.
 * @apiSuccess  {String}                            [conversation.readStatus.readAt]    Message reading time in ISO string.
 *
 */
router.get("/:roomId", controller.onGet);

/**
 * @api {post} /v1/rooms/initiate               create room
 * @apiName                                     CreateRoom
 * @apiDescription                              Create a room.
 * @apiGroup                                    Rooms
 * @apiVersion                                  0.1.0
 * @apiPermission                               admin
 * @apiUse admin
 *
 * @apiBody {String[]}                                userIds               List of user id partecipants.
 * @apiBody {String="advisor-to-customer"}            type                  Room type.
 *
 * @apiSuccess  {Boolean}                             success               Authorized?
 * @apiSuccess  {String}                              room                  Room data.
 * @apiSuccess  {Boolean}                             room.isNew            It is a new room?
 * @apiSuccess  {String}                              room.message          Message result.
 * @apiSuccess  {String="advisor-to-customer"}        type                  Room type.
 *
 */
router.post(
  "/initiate",
  [role.isAdmin, validate(validation.initiate)],
  controller.onInitiate
);

/**
 * @api {post} /v1/rooms/:roomId/message        create message
 * @apiName                                     CreateMessage
 * @apiDescription                              Create a text message.
 * @apiGroup                                    Rooms
 * @apiVersion                                  0.1.0
 * @apiPermission                               authenticated
 * @apiUse authenticated
 *
 * @apiQuery    {String}                            roomId                   Room ID.
 *
 * @apiBody     {String}                            messageText              Message text.
 *
 * @apiSuccess  {Boolean}                           success                      Authorized?
 * @apiSuccess  {String}                            message                      Message data.
 * @apiSuccess  {String}                            message._id                  Message ID.
 * @apiSuccess  {String}                            message.roomId               Room ID.
 * @apiSuccess  {Object}                            message.message              Message object.
 * @apiSuccess  {String}                            message.message.messageText  Message Text.
 * @apiSuccess  {String}                            message.type                 Message Type.
 * @apiSuccess  {[User](#api-Models-ObjectUser)}    message.from                 Message sender.
 * @apiSuccess  {Object[]}                          message.readStatus           List of message reader.
 * @apiSuccess  {String}                            message.readStatus.readBy    Message Reader ID.
 * @apiSuccess  {String}                            message.readStatus.readAt    Message reading time in ISO string.
 *
 */
router.post(
  "/:roomId/message",
  validate(validation.message),
  controller.onPostMessage
);

/**
 * @api {post} /v1/rooms/:roomId/document           create document
 * @apiName                                         CreateDocument
 * @apiDescription                                  Create a document (photo) message. Likely is a photo taken from app camera or phone library.
 * @apiGroup                                        Rooms
 * @apiVersion                                      0.1.0
 * @apiPermission                                   authenticated
 * @apiUse authenticated
 *
 * @apiQuery    {String}                            roomId                   Room ID.
 *
 * @apiBody     {File}                              image                    Image File.
 *
 * @apiSuccess  {Boolean}                           success                     Authorized?
 * @apiSuccess  {String}                            message                      Message data.
 * @apiSuccess  {String}                            message._id                  Message ID.
 * @apiSuccess  {String}                            message.roomId               Room ID.
 * @apiSuccess  {Object}                            message.message              Message object.
 * @apiSuccess  {String}                            message.message.messageText  Message Text.
 * @apiSuccess  {String}                            message.type                 Message Type.
 * @apiSuccess  {[User](#api-Models-ObjectUser)}    message.from                 Message sender.
 * @apiSuccess  {Object[]}                          message.readStatus           List of message reader.
 * @apiSuccess  {String}                            message.readStatus.readBy    Message Reader ID.
 * @apiSuccess  {String}                            message.readStatus.readAt    Message reading time in ISO string.
 *
 */
router.post(
  "/:roomId/document",
  document.single("image"),
  controller.onPostDocument
);

/**
 * @api {post} /v1/rooms/:roomId/pdf                create pdf
 * @apiName                                         CreatePdf
 * @apiDescription                                  Create a prospect pdf message.
 * @apiGroup                                        Rooms
 * @apiVersion                                      0.1.0
 * @apiPermission                                   authenticated
 * @apiUse authenticated
 */

router.post("/:roomId/pdf", pdf.single("pdf"), controller.onPostPdf);

/**
 * @api {put} /v1/rooms/:roomId/mark-read               mark room as read
 * @apiName                                             ReadConversation
 * @apiDescription                                      Mark all conversation message as read.
 * @apiGroup                                            Rooms
 * @apiVersion                                          0.1.0
 * @apiPermission                                       authenticated
 * @apiUse authenticated
 *
 * @apiQuery    {String}                            roomId                         Room ID.
 *
 * @apiBody     {File}                              image                          Image File.
 *
 * @apiSuccess  {Boolean}                           success                        Authorized?
 * @apiSuccess  {Object}                            data                           Operation information.
 * @apiSuccess  {Object}                            data.result                    Result information.
 * @apiSuccess  {Boolean}                           data.result.acknowledged       Is the operation went ok?
 * @apiSuccess  {Number}                            data.result.modifiedCount      Count of modified read message.
 * @apiSuccess  {Number}                            data.result.matchedCount       Count of matched read message.
 * @apiSuccess  {String}                            data.roomId                    Room ID.
 * @apiSuccess  {Object[]}                          data.readByRecipient           List of message reader.
 * @apiSuccess  {String}                            data.readByRecipient.readBy    Message Reader ID.
 * @apiSuccess  {String}                            data.readByRecipient.readAt    Message reading time in ISO string.
 *
 */
router.put("/:roomId/mark-read", controller.onMarkRead);

/**
 * @api {put} /v1/rooms/advisor                 replace room advisor
 * @apiName                                     ReplaceAdvisor
 * @apiDescription                              Replace advisor in room.
 * @apiGroup                                    Rooms
 * @apiVersion                                  0.1.0
 * @apiPermission                               admin
 * @apiUse admin
 *
 * @apiBody    {String}                roomId                  Room ID.
 * @apiBody    {String}                oldAdvisor              Current Advisor in the room.
 * @apiBody    {String}                newAdvisor              New Advisor in the room.
 *
 * @apiSuccess  {Boolean}                success                        Authorized?
 */
router.put(
  "/advisor",
  [role.isAdmin, validate(validation.replaceAdvisor)],
  controller.onChangeAdvisor
);

export default router;
