import Joi from "joi";
const types = ["advisor-to-advisor", "advisor-to-customer"];

export default {
  initiate: {
    body: Joi.object({
      userIds: Joi.array().items(Joi.string()).required(),
      type: Joi.any()
        .valid(...types)
        .required()
    })
  },
  message: {
    body: Joi.object({
      messageText: Joi.string().required()
    })
  },
  replaceAdvisor: {
    body: Joi.object({
      roomId: Joi.string().required(),
      oldAdvisor: Joi.string().required(),
      newAdvisor: Joi.string().required()
    })
  }
};
