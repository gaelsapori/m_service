import service from "./room.service.js";
import userService from "../user/user.service.js";
import firebaseService from "../firebase/firebase.service.js";
import { handler as errorHandler } from "../../middlewares/error.js";

export default {
  roomCheck: async (req, res, next) => {
    try {
      let recipient;
      const { roomId } = req.params;
      const currentUser = await userService.get(req.userId);
      recipient = await service.recipient(roomId, req.userId);

      const room = await service.find(roomId, currentUser);
      req.locals = { recipient, room: room.transform() };
      return next();
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onInitiate: async (req, res) => {
    try {
      const { userIds, type } = req.body;
      const room = await service.initiate(
        [...userIds, req.userId],
        type,
        req.userId
      );
      return res.json({ success: true, room });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onPostMessage: async (req, res) => {
    try {
      if (req.locals.recipient && req.locals.recipient.enabled === false) {
        console.log(req.locals.recipient.referente);
      }
      const { roomId } = req.params;
      const currentUser = await userService.get(req.userId);
      const message = await service.message(
        roomId,
        { messageText: req.body.messageText },
        currentUser.id,
        service.types.TYPE_TEXT
      );

      global.io.sockets.in(roomId).emit("new", { message });
      if (req.locals.recipient && req.locals.recipient.fcmToken) {
        await firebaseService.message({
          title: currentUser.firstName + ", Consulente mutui",
          body: req.body.messageText,
          data: { cta: "", target: "chat", force: "off" },
          token: req.locals.recipient.fcmToken
        });
      }

      return res.json({ success: true, message });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onPostPdf: async (req, res) => {
    try {
      const { roomId } = req.params;
      const cleanName = req.file.originalname.replace(/_/g, " ");
      const description = cleanName.substring(0, cleanName.length - 4);
      const message = await service.message(
        roomId,
        {
          messageText: req.file.filename,
          description: description
        },
        req.userId,
        service.types.TYPE_PDF
      );
      global.io.sockets.in(roomId).emit("new", { message });
      if (req.locals.recipient.fcmToken) {
        await firebaseService.message({
          title: "Nuovo Prospetto Mutuo",
          body: "È disponibile un nuovo prospetto per te. " + description,
          data: { cta: "", target: "chat", force: "off" },
          token: req.locals.recipient.fcmToken
        });
      }

      return res.json({ success: true, message });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onPostDocument: async (req, res) => {
    try {
      const { roomId } = req.params;
      const currentLoggedUser = req.userId;
      const message = await service.message(
        roomId,
        { messageText: req.file.filename },
        currentLoggedUser,
        service.types.TYPE_IMAGE
      );
      global.io.sockets.in(roomId).emit("new", { message });

      return res.json({ success: true, message });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onRecentRooms: async (req, res) => {
    try {
      const currentUser = await userService.get(req.userId);
      const recentRooms = await service.recentRooms(currentUser);
      return res.json({ success: true, rooms: recentRooms });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onListUnreadRooms: async (req, res) => {
    try {
      //const currentUser = await userService.get(req.userId);
      const recentRooms = await service.roomUnread();
      return res.json({ success: true, rooms: recentRooms });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onGet: async (req, res) => {
    try {
      const { roomId } = req.params;
      const recipient = req.locals.recipient;
      const room = req.locals.room;
      const options = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10
      };
      const conversation = await service.conversation(roomId, options);
      const msg = await service.unread(roomId, req.userId);

      return res.json({
        success: true,
        unread: msg.unread,
        total: msg.total,
        room: { ...room, recipient },
        conversation
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onMarkRead: async (req, res) => {
    try {
      const { roomId } = req.params;
      const result = await service.markRead(roomId, req.userId);
      const data = {
        result: result,
        roomId: roomId,
        readByRecipient: {
          readAt: new Date().toISOString(),
          readBy: req.userId
        }
      };

      global.io.sockets.in(roomId).emit("ack", data);
      return res.json({ success: true, data });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onChangeAdvisor: async (req, res) => {
    try {
      const { roomId, oldAdvisor, newAdvisor } = req.body;
      const room = await service.checkReplace(roomId, oldAdvisor);
      await userService.findOneBy({ _id: oldAdvisor, type: "advisor" }, true);
      await userService.findOneBy({ _id: newAdvisor, type: "advisor" }, true);
      room.userIds = room.userIds.map(function (item) {
        return item == oldAdvisor ? newAdvisor : item;
      });
      await service.replace(room._id, room.userIds);

      return res.json({ success: true });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
