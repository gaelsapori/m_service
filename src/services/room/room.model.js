import mongoose from "mongoose";
import { v4 as uuidv4 } from "uuid";

export const CHAT_ROOM_TYPES = {
  ADVISOR_TO_ADVISOR: "advisor-to-advisor",
  ADVISOR_TO_CUSTOMER: "advisor-to-customer"
};

const types = ["advisor-to-advisor", "advisor-to-customer"];

const roomSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4().replace(/-/g, "")
    },
    userIds: Array,
    type: String,
    chatInitiator: String
  },
  {
    timestamps: true,
    collection: "rooms"
  }
);

roomSchema.method({
  transform() {
    const transformed = {};
    const fields = ["_id", "createdAt"];

    fields.forEach((field) => {
      transformed[field] = this[field];
    });

    return transformed;
  }
});

roomSchema.statics = {
  types,
  async initiateChat(userIds, type, chatInitiator) {
    const availableRoom = await this.findOne({
      userIds: {
        $size: userIds.length,
        $all: [...userIds]
      },
      type
    });
    if (availableRoom) {
      return {
        isNew: false,
        message: "Stanza già esistente",
        roomId: availableRoom._doc._id,
        type: availableRoom._doc.type
      };
    }

    const newRoom = await this.create({ userIds, type, chatInitiator });
    return {
      isNew: true,
      message: "Nuova stanza creata",
      roomId: newRoom._doc._id,
      type: newRoom._doc.type
    };
  },
  async getRoomWithPartecipant(user) {
    const aggregate = await this.aggregate([
      { $match: { userIds: { $all: [user._id] } } },
      { $limit: 1 },
      {
        $lookup: {
          from: "users",
          localField: "userIds",
          foreignField: "_id",
          as: "roomPartecipant"
        }
      },
      { $unwind: "$roomPartecipant" },
      {
        $match: {
          "roomPartecipant._id": { $ne: user._id },
          "roomPartecipant.type": { $ne: user.type },
          "roomPartecipant.type": { $ne: "admin" }
        }
      },
      {
        $group: {
          _id: "$_id",
          type: { $last: "$type" },
          recipient: { $last: "$roomPartecipant" },
          chatInitiator: { $first: "$chatInitiator" },
          createdAt: { $first: "$createdAt" }
        }
      }
    ]);
    return aggregate[0];
  },
  async getRoomRecipient(roomId, currentLoggedUser) {
    const aggregate = await this.aggregate([
      { $match: { _id: roomId } },
      {
        $lookup: {
          from: "users",
          localField: "userIds",
          foreignField: "_id",
          as: "roomPartecipant"
        }
      },
      { $unwind: "$roomPartecipant" },
      {
        $match: {
          "roomPartecipant._id": { $ne: currentLoggedUser }
        }
      },
      {
        $group: {
          _id: "$_id",
          recipient: {
            $last: {
              id: "$roomPartecipant._id",
              firstName: "$roomPartecipant.firstName",
              lastName: "$roomPartecipant.lastName",
              fcmToken: "$roomPartecipant.fcmToken",
              type: "$roomPartecipant.type",
              enabled: "$roomPartecipant.enabled",
              referente: "$roomPartecipant.referente"
            }
          }
        }
      },
      {
        $project: {
          _id: 0,
          recipient: 1
        }
      }
    ]);

    return aggregate[0];
  },
  async getRecentRooms(user) {
    const aggregate = await this.aggregate([
      { $match: { userIds: { $all: [user._id] } } },
      {
        $lookup: {
          from: "users",
          localField: "userIds",
          foreignField: "_id",
          as: "recipient"
        }
      },
      { $unwind: "$recipient" },
      {
        $match: {
          "recipient._id": { $ne: user._id },
          "recipient.type": { $ne: user.type },
          "recipient.type": { $ne: user.type == "admin" ? "advisor" : "admin" }
        }
      },

      {
        $lookup: {
          from: "messages",
          localField: "_id",
          foreignField: "roomId",
          as: "messages"
        }
      },

      { $unwind: { path: "$messages", preserveNullAndEmptyArrays: true } },
      { $unwind: { path: "$messages.type", preserveNullAndEmptyArrays: true } },
      {
        $unwind: { path: "$messages.message", preserveNullAndEmptyArrays: true }
      },
      {
        $unwind: {
          path: "$messages.createdAt",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: "users",
          localField: "messages.from",
          foreignField: "_id",
          as: "from"
        }
      },
      { $unwind: { path: "$from", preserveNullAndEmptyArrays: true } },
      {
        $project: {
          isUnread: {
            $cond: {
              if: { $isArray: "$messages.readStatus" },
              then: {
                $cond: [
                  {
                    $size: {
                      $filter: {
                        input: "$messages.readStatus",
                        cond: {
                          $eq: ["$$this.readBy", user._id]
                        }
                      }
                    }
                  },
                  0,
                  1
                ]
              },
              else: 0
            }
          },
          messages: 1,
          from: {
            $ifNull: [
              {
                _id: "$from._id",
                firstName: "$from.firstName",
                lastName: "$from.lastName"
              },
              null
            ]
          },
          type: 1,
          messageSentAt: 1,
          readStatus: 1,
          recipient: {
            _id: 1,
            image: 1,
            firstName: 1,
            lastName: 1,
            email: 1,
            phone: 1,
            leadId: 1
          },
          chatInitiator: 1,
          createdAt: 1
        }
      },
      {
        $group: {
          _id: "$_id",
          roomId: { $last: "$_id" },
          recipient: { $last: "$recipient" },
          message: { $last: "$messages.message" },
          from: { $last: "$from" },
          type: { $last: "$messages.type" },
          readStatus: { $last: "$messages.readStatus" },
          chatInitiator: { $first: "$chatInitiator" },
          createdAt: { $first: "$createdAt" },
          messageSentAt: { $last: "$messages.createdAt" },
          unreadMessages: { $sum: "$isUnread" }
        }
      },
      { $sort: { messageSentAt: -1, createdAt: -1 } }
    ]);
    return aggregate;
  },
  async updateRoomPartecipants(id, partecipants) {
    return await this.findOneAndUpdate(
      { _id: id },
      { userIds: partecipants },
      { upsert: false, new: true }
    );
  },
  async getAdvisorRooms(advisor) {
    const aggregate = await this.aggregate([
      { $match: { userIds: { $all: [advisor._id] } } },
      {
        $lookup: {
          from: "users",
          localField: "userIds",
          foreignField: "_id",
          as: "roomPartecipant"
        }
      },
      { $unwind: "$roomPartecipant" },
      {
        $match: {
          "roomPartecipant._id": { $ne: advisor._id },
          "roomPartecipant.type": { $ne: advisor.type },
          "roomPartecipant.type": { $ne: "admin" }
        }
      },
      {
        $group: {
          _id: "$_id",
          type: { $last: "$type" },
          customer: { $last: "$roomPartecipant" },
          chatInitiator: { $first: "$chatInitiator" },
          createdAt: { $first: "$createdAt" }
        }
      },
      {
        $project: {
          _id: 0,
          roomId: "$_id",
          customer: {
            _id: 1,
            leadId: 1,
            firstName: 1,
            lastName: 1,
            email: 1,
            phone: 1
          }
        }
      }
    ]);
    return aggregate;
  },
  async listUnreads() {
    const aggregate = await this.aggregate([
      /*{ $match: { userIds: { $all: [user._id] } } },*/
      {
        $lookup: {
          from: "users",
          localField: "userIds",
          foreignField: "_id",
          as: "recipient"
        }
      },
      /*{ $unwind: "$recipient" },
      {
        $match: {
          "recipient.type": { $ne: "advisor" }
        }
      },*/

      {
        $lookup: {
          from: "messages",
          localField: "_id",
          foreignField: "roomId",
          as: "messages"
        }
      },

      { $unwind: { path: "$messages", preserveNullAndEmptyArrays: true } },
      {
        $lookup: {
          from: "users",
          localField: "messages.from",
          foreignField: "_id",
          as: "from"
        }
      },
      { $unwind: { path: "$from", preserveNullAndEmptyArrays: true } },
      {
        $project: {
          isUnread: {
            $cond: {
              if: { $isArray: "$messages.readStatus" },
              then: {
                $cond: [
                  {
                    $size: {
                      $filter: {
                        input: "$messages.readStatus",
                        cond: {
                          $eq: ["$$this.readBy", "$recipient._id"]
                        }
                      }
                    }
                  },
                  0,
                  1
                ]
              },
              else: 0
            }
          },
          messages: 1,
          //type: 1,
          messageSentAt: 1,
          recipient: {
            _id: 1,
            firstName: 1,
            lastName: 1,
            email: 1,
            phone: 1,
            leadId: 1,
            type: 1
          },
          createdAt: 1
        }
      },
      {
        $group: {
          _id: "$_id",
          roomId: { $last: "$_id" },
          recipient: { $last: "$recipient" },
          //message: { $last: "$messages.message" },
          //type: { $last: "$messages.type" },
          //createdAt: { $first: "$createdAt" },
          messageSentAt: { $last: "$messages.createdAt" },
          unreadMessages: { $sum: "$isUnread" }
        }
      },
      { $match: { unreadMessages: { $gt: 0 } } },
      { $sort: { messageSentAt: -1, createdAt: -1 } }
    ]);
    return aggregate;
  }
};

export default mongoose.model("Room", roomSchema);

/**
 * @api {OBJECT} Room Room
 * @apiGroup Models
 * @apiVersion 0.1.0
 * @apiParam {String}                         _id               Room ID.
 * @apiParam {String="advisor-to-customer"}   type              Room type.
 * @apiParam {String}                         chatInitiator     User ID of whom initiate the chat.
 * @apiParam {String[]}                       userIds           Room partecipants IDs.
 * @apiParam {String}                         createdAt         Room creation date in ISO string.
 */
