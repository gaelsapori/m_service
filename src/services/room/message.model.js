import mongoose from "mongoose";
import { v4 as uuidv4 } from "uuid";

export const MESSAGE_TYPES = {
  TYPE_TEXT: "text",
  TYPE_IMAGE: "image",
  TYPE_PDF: "pdf"
};

const readByRecipientSchema = new mongoose.Schema(
  {
    _id: false,
    readBy: String,
    readAt: {
      type: Date,
      default: Date.now()
    }
  },
  {
    timestamps: false
  }
);

const messageSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4().replace(/-/g, "")
    },
    roomId: String,
    message: mongoose.Schema.Types.Mixed,
    type: {
      type: String
    },
    from: String,
    readStatus: [readByRecipientSchema]
  },
  {
    timestamps: true,
    collection: "messages"
  }
);

messageSchema.statics = {
  types: MESSAGE_TYPES,
  async createMsgInRoom(roomId, message, from, type) {
    const msg = await this.create({
      roomId,
      message,
      from,
      type,
      readStatus: { readBy: from }
    });

    const aggregate = await this.aggregate([
      { $match: { _id: msg._id } },
      {
        $lookup: {
          from: "users",
          localField: "from",
          foreignField: "_id",
          as: "from"
        }
      },
      { $unwind: "$from" }
    ]);
    return aggregate[0];
  },
  async getConversationByRoomId(roomId, options = {}) {
    return this.aggregate([
      { $match: { roomId } },
      {
        $lookup: {
          from: "users",
          localField: "from",
          foreignField: "_id",
          as: "from"
        }
      },
      { $unwind: "$from" },
      {
        $project: {
          _id: 1,
          roomId: 1,
          message: 1,
          type: 1,
          readStatus: 1,
          createdAt: 1,
          from: {
            _id: 1,
            firstName: 1,
            lastName: 1,
            type: 1
          }
        }
      },
      { $skip: options.page * options.limit },
      { $limit: options.limit },
      { $sort: { createdAt: 1 } }
    ]);
  },
  async markMessageRead(roomId, currentUserOnlineId) {
    return this.updateMany(
      {
        roomId,
        "readStatus.readBy": { $ne: currentUserOnlineId }
      },
      {
        $addToSet: {
          readStatus: { readBy: currentUserOnlineId }
        }
      },
      {
        multi: true
      }
    );
  },
  async getUnreadRoomMessage(roomId, currentUserOnlineId) {
    let aggregate = await this.aggregate([
      { $match: { roomId } },
      {
        $project: {
          _id: 0,
          isUnread: {
            $cond: [{ $in: [currentUserOnlineId, "$readStatus.readBy"] }, 0, 1]
          }
        }
      },
      {
        $group: {
          _id: "$roomId",
          unreadMessages: { $sum: "$isUnread" },
          messageCount: { $sum: 1 }
        }
      },
      {
        $group: {
          _id: "$roomPartecipant._id",
          unreadMessages: { $last: "$unreadMessages" },
          messageCount: { $last: "$messageCount" }
        }
      },
      {
        $project: {
          _id: 0,
          unreadMessages: 1,
          messageCount: 1
        }
      }
    ]);
    return aggregate[0];
  }
};

export default mongoose.model("Message", messageSchema);
