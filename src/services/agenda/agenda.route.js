import express from "express";
import { validate } from "express-validation";
import controller from "./agenda.controller.js";
import validation from "./agenda.validation.js";
import role from "../../middlewares/role.js";
const router = express.Router();

/**
 * @api {post} /v1/agenda                           create appointment
 * @apiName                                         CreateAppointment
 * @apiDescription                                  Create an appointment.
 * @apiGroup                                        Agenda
 * @apiVersion                                      0.1.0
 * @apiPermission                                   authenticated
 * @apiUse authenticated
 *
 * @apiBody     {String}       bank                 Bank code.
 * @apiBody     {String}       researchStage        Research stage.
 * @apiBody     {String}       date                 Appointment date.
 * @apiBody     {String}       time                 Appointment time.
 *
 * @apiSuccess  {Boolean}         success      Authorized?
 * @apiSuccess  {String}          message      Message response
 *
 */
router.post(
  "/",
  [role.isCustomer, validate(validation.appointment)],
  controller.onCreate
);

/**
 * @api {get} /v1/agenda/crm                        get crm appointments
 * @apiName                                         GetCRMAppointments
 * @apiDescription                                  GEt appointments confirmed from CRM.
 * @apiGroup                                        Agenda
 * @apiVersion                                      0.1.0
 * @apiPermission                                   authenticated
 * @apiUse authenticated
 *
 * @apiSuccess  {Boolean}                                       success             Authorized?
 * @apiSuccess  {[Prospect[]](#api-Models-ObjectProspect)}      appointments        List of prospects with appointment confirmed.
 *
 */
router.get("/crm", controller.onList);

export default router;
