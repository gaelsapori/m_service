import service from "./agenda.service.js";
import userService from "../user/user.service.js";
import CrmLookup from "../crm/crm.lookup.js";
import User from "../user/user.model.js";
import { handler as errorHandler } from "../../middlewares/error.js";

export default {
  onCreate: async (req, res) => {
    try {
      const user = await userService.get(req.userId);
      const { bank, researchStage, date, time } = req.body;
      const advisor = await userService.findOneAdvisor(user.advisorId);
      const datetime = service.datetime(date, time);
      const appointment = await service.create({
        bank,
        researchStage,
        datetime,
        user: user.id,
        advisor: advisor._id,
        confirmed: false
      });

      await service.mail(appointment, time, advisor, user);
      return res.json({
        success: true,
        message: `Richiesta di prenotazione inviata`
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onList: async (req, res) => {
    try {
      const currentLoggedUser = req.userId;
      const user = await User.get(currentLoggedUser);
      const prospects = await CrmLookup.myProspects(user.leadId, true);
      const appointments = service.transform(prospects);

      return res.json({
        success: true,
        appointments
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
