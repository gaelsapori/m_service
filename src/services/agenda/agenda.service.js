import Agenda from "./agenda.model.js";
import mailer from "../../utils/mailer.js";

export default {
  create: async (agenda) => {
    return await Agenda.create(agenda);
  },
  datetime: (date, time) => {
    const t = time.split("-");
    return new Date(
      Date.parse(`${date.split("/").reverse().join("-")} ${t[0]}`)
    );
  },
  mail: async (appointment, time, advisor, user) => {
    return await mailer.sendEmailNotificationForAppointment(
      appointment,
      time,
      advisor,
      user
    );
  },
  transform: (prospects) => {
    if (prospects.length) {
      for (const a in prospects) {
        prospects[a].extra_spese = JSON.parse(prospects[a].extra_spese);
        prospects[a].isNew = true;
        prospects[a].tan = parseFloat(prospects[a].tan);
        prospects[a].taeg = parseFloat(prospects[a].taeg);
        prospects[a].pagamento_mensile = parseFloat(
          prospects[a].pagamento_mensile
        );
        prospects[a].reddito_mensile_netto = parseFloat(
          prospects[a].reddito_mensile_netto
        );
        prospects[a].prezzo_immobile = parseFloat(prospects[a].prezzo_immobile);
        prospects[a].valore_mutuo = parseFloat(prospects[a].valore_mutuo);
      }
    }
    return prospects;
  }
};
