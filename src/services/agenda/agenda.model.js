import mongoose from "mongoose";
import { v4 as uuidv4 } from "uuid";

const agendaSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4().replace(/-/g, "")
    },
    bank: {
      type: String,
      required: true
    },
    researchStage: {
      type: String,
      required: true
    },
    datetime: {
      type: Date,
      required: true
    },
    rdv: {
      type: Date
    },
    advisor: { type: String, ref: "User" },
    user: { type: String, ref: "User" },
    confirmed: { type: Boolean }
  },
  {
    timestamps: true,
    collection: "agenda"
  }
);

export default mongoose.model("Agenda", agendaSchema);
