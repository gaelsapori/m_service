import Joi from "joi";

export default {
  // POST /v1/agenda
  appointment: {
    body: Joi.object({
      bank: Joi.string().required(),
      researchStage: Joi.string().required(),
      date: Joi.string().required(),
      time: Joi.string().required()
    })
  }
};
