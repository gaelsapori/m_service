import Joi from "joi";
const types = ["admin", "advisor", "editor", "customer", "survey", "budget"];

export default {
  user: {
    body: Joi.object({
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      phone: Joi.string().required(),
      email: Joi.string().email().required(),
      type: Joi.string()
        .valid(...types)
        .required(),
      advisorId: Joi.string().required(),
      leadId: Joi.string(),
      otpVerified: Joi.string(),
      supportedUsers: Joi.string(),
      fcmToken: Joi.string(),
      sex: Joi.string(),
      age: Joi.string()
    })
  },
  search: {
    query: Joi.object({
      page: Joi.string().min(0),
      limit: Joi.string().min(1).max(100),
      type: Joi.string().valid(...types),
      enabled: Joi.string()
    })
  }
};
