import httpStatus from "http-status";
import service from "./user.service.js";
import roomService from "../room/room.service.js";
import ratingService from "../rating/rating.service.js";
import CrmLookup from "../crm/crm.lookup.js";
import User from "./user.model.js";
import hashUtil from "../../utils/hash.js";
import APIError from "../../utils/api.error.js";
import { handler as errorHandler } from "../../middlewares/error.js";

export default {
  onLoad: async (req, res, next, id) => {
    try {
      const user = await service.get(id);
      req.locals = { user };
      return next();
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onGet: async (req, res) => {
    try {
      const survey = await CrmLookup.survey("telefono", req.locals.user.phone);
      return res.json({
        success: true,
        user: req.locals.user.transform(),
        survey
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onList: async (req, res, next) => {
    try {
      const user = await service.get(req.userId);

      const options = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10,
        type: req.query.type
      };

      if (req.query.enabled) {
        options.enabled = req.query.enabled == "true";
      }

      if (user.type == "advisor") {
        options.referente = req.userId;
      }

      const response = await service.list(options);
      return res.json({ success: true, ...response });
    } catch (error) {
      next(error);
    }
  },
  onGetRooms: async (req, res) => {
    try {
      const filterUnread = req.query.unread == "true";
      const options = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10,
        type: req.query.type
      };

      if (filterUnread) {
        options.page = 0;
        options.limit = 500;
      }

      let response = await service.findRooms(req.params.id, options);

      let rooms = [];

      for (let index = 0; index < response.rooms.length; index++) {
        const element = response.rooms[index].roomPartecipants[0];
        let msg = await roomService.unread(element.roomId, req.params.id);
        element.unread = msg.unread;
        element.total = msg.total;
        if (filterUnread) {
          if (msg.unread > 0) rooms.push(element);
        } else {
          rooms.push(element);
        }
      }
      if (filterUnread) {
        const r = response.rooms.filter(
          (u) => u.roomPartecipants[0].unread > 0
        );
        response.rooms = r;
        response.total = rooms.length;
      }

      return res.json({ success: true, ...response });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onCreate: async (req, res) => {
    try {
      const user = await service.create(req.body);
      return res.json({ success: true, user });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onUpdate: async (req, res) => {
    try {
      await service.canUpdate(req.userId, req.params.id);
      if (req.file) req.body.image = req.file.filename;
      req.body._id = req.params.id;
      const user = await service.update(req.body);
      return res.json({ success: true, user });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onDelete: async (req, res) => {
    try {
      const user = await service.delete(req.params.id);
      return res.json({
        success: true,
        message: `Deleted a count of ${user.deletedCount} user.`
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onMe: async (req, res) => {
    try {
      let user, recentRooms, rating, advisor, room;
      user = await service.get(req.userId);
      recentRooms = await roomService.recentRooms(user);
      const agentData = await CrmLookup.leadAdvisor(user.leadId);

      if (agentData) {
        const agent = service.transformAgent(agentData);
        advisor = await service.findOneAdvisor(agent.advisorId, false);
        if (advisor) {
          agent._id = advisor._id;
          advisor = await service.update(agent);
        }
        rating = await ratingService.get(advisor._id, ["$userId", user._id]);
        if (user.exported != true) {
          const leadExists = await CrmLookup.exportedPhone(user.phone);
          user.exported = leadExists != null;
        } else {
          user.advisorId = agent.advisorId;
          service.update(user);
        }
      }
      if ([User.types.CUSTOMER, User.types.BUDGET].includes(req.userType)) {
        room = recentRooms[0] ?? null;
        if (user.referralCode == null) {
          const code = hashUtil.referralCode();
          user.referralCode = code;
          service.update(user);
        }
        if (room && room.recipient && room.recipient._id != advisor._id) {
          room = await roomService.replace(room._id, [advisor._id, user._id]);
        }
        if (advisor && advisor.enabled == false) {
          let agentId = advisor.referente;
          const mostRecent = await CrmLookup.findMostRecentExported(user.phone);

          if (mostRecent) {
            user.leadId = mostRecent[0].leadId;
            advisor = await service.findOneAdvisor(
              mostRecent[0].advisorId,
              false
            );
            agentId = advisor.id;
          } else {
            advisor = await service.get(agentId);
          }

          room = await roomService.replace(room._id, [advisor._id, user._id]);
          user.advisorId = advisor.advisorId;
          service.update(user);
        }
      }

      return res.json({
        success: true,
        user: user.transform(),
        advisor: advisor,
        room,
        rating
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onEnquire: async (req, res) => {
    try {
      let user, room, rating, advisor;
      user = await service.get(req.userId);
      const surveyData = await service.enquire(user.phone);
      const exportedUser = await CrmLookup.exportedPhone(user.phone);
      surveyData.exported = exportedUser != null;
      const customer = service.transformSurvey(surveyData);
      customer._id = req.userId;
      user = await service.update(customer, false);

      const agentData = await CrmLookup.leadAdvisor(customer.leadId);
      const agent = service.transformAgent(agentData);
      advisor = await service.findOneAdvisor(agent.advisorId);

      if (!advisor) {
        advisor = await service.create(agent);
      } else {
        agent._id = advisor._id;
        advisor = await service.update(agent);
      }

      await roomService.initiate(
        [user._id, advisor._id],
        "advisor-to-customer",
        advisor._id
      );
      room = await roomService.recentRooms(user);
      rating = await ratingService.get(advisor._id, ["$userId", user._id]);

      return res.json({
        success: true,
        user: user.transform(),
        advisor,
        room: room != null ? room[0] : null,
        rating,
        token: user.token()
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onSync: async (req, res) => {
    try {
      let userId = req.query.id ? req.query.id : req.userId;
      let advisor = await service.findOneBy(
        { _id: userId, type: User.types.ADVISOR },
        true
      );
      const agentData = await CrmLookup.advisor(advisor.advisorId);
      const agent = service.transformAgent(agentData);
      agent._id = advisor._id;
      advisor = await service.update(agent);

      return res.json({ success: true, user: advisor });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onFullSync: async (req, res) => {
    try {
      let advisor = await service.findOneBy(
        { _id: req.params.id, type: User.types.ADVISOR },
        true
      );

      // aggiorno i dati advisor
      const agentData = await CrmLookup.advisor(advisor.advisorId);
      const agent = service.transformAgent(agentData);
      agent._id = advisor._id;
      advisor = await service.update(agent);

      // aggiorno stanze e utenti
      await service.syncAdvisorCustomers(advisor);

      return res.json({ success: true, advisor });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onRedeem: async (req, res) => {
    try {
      const { referralCode, userId } = req.body;
      let added = false;
      let message = "Il codice è già stato registrato";
      let user = await service.findOneBy({ referralCode: referralCode });
      if (user == null) {
        throw new APIError({
          message: "Codice non valido.",
          status: httpStatus.NOT_FOUND
        });
      }
      if (!user.referrals.includes(userId)) {
        message = "Il referral è stato registrato";
        user.referrals.push(userId);
        added = true;
        user = await service.update(user);
      }

      return res.json({
        success: true,
        message,
        referrals: user.referrals.length,
        added
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onNotify: async (req, res) => {
    try {
      service.notify(req.body.message, req.params.id);
      return res.json({ success: true });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
