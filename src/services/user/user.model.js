import { v4 as uuidv4 } from "uuid";
import lodash from "lodash";
const { omitBy, isNil } = lodash;
import mongoose from "mongoose";
import envar from "../../config/vars.js";
import APIError from "../../utils/api.error.js";
import httpStatus from "http-status";
import CrmLookup from "../crm/crm.lookup.js";
import hashUtil from "../../utils/hash.js";
import jwt from "jsonwebtoken";

const types = ["admin", "advisor", "editor", "customer", "survey", "budget"];

export const USER_TYPES = {
  CUSTOMER: "customer",
  SURVEY: "survey",
  ADVISOR: "advisor",
  EDITOR: "editor",
  ADMIN: "admin",
  BUDGET: "budget"
};

const userSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4().replace(/-/g, "")
    },
    phone: {
      type: String,
      required: true
    },
    image: {
      type: String,
      default: ""
    },
    firstName: String,
    lastName: String,
    email: {
      type: String,
      match: /^\S+@\S+\.\S+$/,
      //required: true,
      //unique: true,
      trim: true,
      lowercase: true
    },
    advisorId: Number,
    leadId: Number,
    supportedUsers: Number,
    fcmToken: String,
    otpVerified: Boolean,
    privacyCheck: Boolean,
    tocCheck: Boolean,
    type: {
      type: String,
      enum: types,
      default: "survey"
    },
    cityId: Number,
    sex: String,
    age: Number,
    birthDate: String,
    city: Object,
    researchStage: String,
    exported: Boolean,
    enabled: {
      type: Boolean,
      default: true
    },
    referente: String,
    referralCode: String,
    referrals: {
      type: Array,
      default: []
    }
  },
  {
    timestamps: true,
    collection: "users"
  }
);

userSchema.method({
  transform() {
    const transformed = {};
    const fields = [
      "_id",
      "phone",
      "image",
      "firstName",
      "lastName",
      "email",
      "advisorId",
      "leadId",
      "type",
      "supportedUsers",
      "referralCode",
      "age",
      "exported",
      "fcmToken",
      "otpVerified",
      "enabled",
      "referente"
    ];

    fields.forEach((field) => {
      transformed[field] = this[field];
    });

    return transformed;
  },

  token(expiration = null) {
    const params = expiration != null ? { expiresIn: expiration } : {};
    return jwt.sign(
      { userId: this._id, userType: this.type },
      envar.jwtSecret,
      params
    );
  }
});

userSchema.statics = {
  types: USER_TYPES,
  async updateUser(obj, transform = true) {
    const user = await this.findOneAndUpdate({ _id: obj._id }, obj, {
      upsert: false,
      new: true
    });
    if (transform == true) return user.transform();
    return user;
  },
  async get(id) {
    const user = await this.findById(id);
    if (!user)
      throw new APIError({
        message: "Nessun utente trovato.",
        status: httpStatus.NOT_FOUND
      });
    return user;
  },

  async findOneUserBy(filter, raiseException = false) {
    const user = await this.findOne(filter);
    if (!user && raiseException === true)
      throw new APIError({
        message: "Nessun utente trovato.",
        status: httpStatus.NOT_FOUND
      });
    return user;
  },
  async list({ page = 0, limit = 10, type, enabled, referente }) {
    let aggregation;
    const options = omitBy({ type, enabled, referente }, isNil);
    const project = {
      _id: 1,
      phone: 1,
      email: 1,
      firstName: 1,
      lastName: 1,
      image: 1,
      advisorId: 1,
      age: 1,
      leadId: 1,
      supportedUsers: 1,
      type: 1,
      createdAt: 1,
      fcmToken: 1,
      otpVerified: 1,
      enabled: 1,
      referente: 1
    };
    const facet = {
      data: [
        { $sort: { createdAt: -1 } },
        { $skip: page * limit },
        { $limit: limit }
      ],
      total: [{ $count: "total" }]
    };
    if (type != "advisor") {
      aggregation = [
        { $match: options },
        { $project: project },
        { $facet: facet }
      ];
    } else {
      aggregation = [
        { $match: options },
        {
          $lookup: {
            from: "rooms",
            localField: "_id",
            foreignField: "userIds",
            as: "rooms"
          }
        },
        { $project: { ...project, rooms: { $size: "$rooms" } } },
        { $facet: facet }
      ];
    }
    const aggregate = await this.aggregate(aggregation);
    return aggregate[0];
  },
  async findAdvisorWithRooms(advisorId, { page = 0, limit = 10 }) {
    const aggregate = await this.aggregate([
      { $match: { _id: advisorId, type: USER_TYPES.ADVISOR } },
      {
        $lookup: {
          from: "rooms",
          localField: "_id",
          foreignField: "userIds",
          as: "rooms"
        }
      },
      { $unwind: "$rooms" },
      {
        $lookup: {
          from: "users",
          localField: "rooms.userIds",
          foreignField: "_id",
          as: "roomPartecipants"
        }
      },

      {
        $project: {
          roomPartecipants: {
            $map: {
              input: {
                $filter: {
                  input: "$roomPartecipants",
                  as: "roomPartecipantf",
                  cond: "$$roomPartecipantf.leadId"
                }
              },
              as: "roomPartecipantm",
              in: {
                _id: "$$roomPartecipantm._id",
                leadId: "$$roomPartecipantm.leadId",
                firstName: "$$roomPartecipantm.firstName",
                lastName: "$$roomPartecipantm.lastName",
                email: "$$roomPartecipantm.email",
                phone: "$$roomPartecipantm.phone",
                roomId: "$rooms._id"
              }
            }
          },
          createdAt: "$rooms.createdAt"
        }
      },
      {
        $facet: {
          data: [
            { $sort: { createdAt: -1 } },
            { $skip: page * limit },
            { $limit: limit }
          ],
          total: [{ $count: "total" }]
        }
      }
    ]);

    return aggregate[0];
  },
  async syncUser(leadId) {
    let user;
    user = await this.findOneUserBy({ leadId: leadId });
    const leadData = await CrmLookup.lead(leadId);
    const age = hashUtil.computeAge(leadData.data_nascita);
    user.email = leadData.email;
    user.firstName = leadData.nome;
    user.lastName = leadData.cognome;
    user.phone = leadData.telefono;
    user.age = age;
    user = await this.updateUser(user);
    return user;
  },
  async findAllAppLeadId() {
    const aggregate = await this.aggregate([
      { $match: { type: USER_TYPES.CUSTOMER } },
      {
        $project: {
          _id: 0,
          leadId: 1
        }
      }
    ]);
    return aggregate;
  }
};

export default mongoose.model("User", userSchema);

/**
 * @apiDefine authenticated
 *
 * @apiHeader {String} Authorization Bearer 5f048fe...
 * @apiHeaderExample {Header} Header-Example
 *     "Authorization: Bearer 5f048fe..."
 * @apiUse authErrors
 */

/**
 * @apiDefine admin
 *
 * @apiHeader {String} Authorization Bearer 5f048fe...
 * @apiHeaderExample {Header} Header-Example
 *     "Authorization: Bearer 5f048fe..."
 * @apiUse authErrors

 * @apiErrorExample {json} Unauthorized-Response:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "success": false,
 *        "message": "Non sei autorizzato"
 *     }
 */

/**
 * @apiDefine authErrors
 *
 * @apiErrorExample {json} NoToken-Response:
 *    HTTP/1.1 400 Bad Request
 *    {
 *       "success": false,
 *       "message": "Nessun token di accesso fornito"
 *    }
 * @apiErrorExample {json} InvalidToken-Response:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "success": false,
 *        "message": "invalid token"
 *     }
 */
/**
 * @api {OBJECT} User User
 * @apiGroup Models
 * @apiVersion 0.1.0
 * @apiParam {String}                                           _id                 User ID.
 * @apiParam {String}                                           phone               Phone number.
 * @apiParam {String}                                           email               Email address.
 * @apiParam {String}                                           firstName           First name.
 * @apiParam {String}                                           lastName            Last name.
 * @apiParam {String="admin", "advisor", "editor", "customer"}  type                User type.
 * @apiParam {String}                                           [image=" "]         Profile image name.
 * @apiParam {String}                                           [advisorId]         Advisor CRM ID. [for **customer type** is its advisor - for **advisor type** is itself].
 * @apiParam {String}                                           [leadId]            Lead CRM ID [only for customer type].
 * @apiParam {Boolean}                                          [otpVerified]       Has it already been OTP verified.
 * @apiParam {String}                                           [fcmToken]          Firebase user device token [only for customer type].
 * @apiParam {String}                                           [supportedUsers]    How many user are supported from advisorId [only for advisor type].
 * @apiParam {String}                                           [sex]               User gender [only for customer type].
 * @apiParam {Number}                                           [age]               User age [only for customer type].
 * @apiParam {String}                                           createdAt           Creation date in ISO string.
 * @apiParam {String}                                           updatedAt           Update date in ISO string.
 */
