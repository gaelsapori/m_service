import httpStatus from "http-status";
import User from "./user.model.js";
import roomService from "../room/room.service.js";
import hashUtil from "../../utils/hash.js";
import redis from "../../utils/redis.js";
import CrmLookup from "../crm/crm.lookup.js";
import Room from "../room/room.model.js";
import APIError from "../../utils/api.error.js";

export default {
  get: async (id) => User.get(id),
  loggedIn: (req, res) => res.json(req.user.transform()),
  create: async (customer, transform = true) => {
    const user = await User.create(customer);
    if (transform == true) return user.transform();
    return user;
  },
  update: async (user, transform = true) => {
    return await User.updateUser(user, transform);
  },
  delete: async (userId) => await User.deleteOne({ _id: userId }),
  list: async (params) => {
    const users = await User.list(params);
    //const transformedUsers = users.map((user) => user.transform());
    return {
      total: users.total[0] ? users.total[0].total : 0,
      users: users.data
    };
  },
  canUpdate: async (userId, toUpdate) => {
    const currentUser = await User.findOneUserBy({ _id: userId }, true);
    await User.findOneUserBy({ _id: toUpdate }, true);
    if (currentUser.type != "admin" && currentUser.id != toUpdate) {
      throw new APIError({
        message: "Non sei autorizzato.",
        status: httpStatus.UNAUTHORIZED
      });
    }
    return true;
  },
  findRooms: async (advisorId, options) => {
    const rooms = await User.findAdvisorWithRooms(advisorId, options);
    return {
      total: rooms.total[0] ? rooms.total[0].total : 0,
      rooms: rooms.data
    };
  },
  findOneAdvisor: async (advisorId, raise) => {
    return await User.findOneUserBy(
      { advisorId: advisorId, type: User.types.ADVISOR },
      raise
    );
  },
  findOneBy: async (filter, raise) => {
    return await User.findOneUserBy(filter, raise);
  },
  findAdvisorEmail: async (email) => {
    const user = await User.findOneUserBy(
      { email: email, type: { $ne: User.types.CUSTOMER } },
      true
    );
    return user;
  },
  syncUser: async (leadId) => {
    return await User.syncUser(leadId);
  },
  syncAdvisorCustomers: async (advisor) => {
    const allAppLeadId = [];
    const advisorCustomers = [];

    // collezioni gli attuali utenti dell'advisor
    const advisorRooms = await Room.getAdvisorRooms(advisor);
    advisorRooms.forEach((room) => advisorCustomers.push(room.customer.leadId));

    // collezioni tutti gli utenti con app
    const ids = await User.findAllAppLeadId();
    ids.forEach((user) => allAppLeadId.push(user.leadId));

    // cerco gli utenti dell'advisor dal crm che hanno l'app
    const supportedLead = await CrmLookup.findLeadExported(
      advisor.advisorId,
      allAppLeadId
    );

    for (var i in supportedLead) {
      const leadId = supportedLead[i].leadId;
      if (advisorCustomers.includes(leadId)) {
        // utente correttamente assegnato, aggiorno i dati utente.
        await User.syncUser(leadId);
      } else {
        // utente non assegnato. cerco e riassegno
        const user = await User.findOneUserBy(
          { leadId: leadId, type: User.types.CUSTOMER },
          true
        );
        const room = await Room.getRoomWithPartecipant(user);

        if (room != null && advisor != null && user != null) {
          await roomService.replace(room._id, [advisor._id, user._id]);
        }

        // aggiorno i dati utente
        await User.syncUser(leadId);
      }
    }
  },
  enquire: async (phone) => {
    const surveyData = await CrmLookup.survey("telefono", phone);
    if (!surveyData) {
      throw new APIError({
        message: "Lead non acora assegnata.",
        status: httpStatus.OK
      });
    }
    return surveyData;
  },
  transformLead(crmUserData) {
    var date = new Date(crmUserData.data_nascita);
    const dataNascita = date.toISOString().substring(0, 10);
    return {
      firstName: crmUserData.nome,
      lastName: crmUserData.cognome,
      type: User.types.CUSTOMER,
      phone: crmUserData.telefono,
      email: crmUserData.email,
      advisorId: crmUserData.advisor_id,
      leadId: crmUserData.id,
      sex: crmUserData.sesso,
      cityId: crmUserData.comune,
      birthDate: dataNascita,
      age: hashUtil.computeAge(crmUserData.data_nascita),
      otpVerified: true,
      exported: true
    };
  },
  transformAgent(crmAgendData) {
    return {
      firstName: crmAgendData.nome,
      lastName: crmAgendData.cognome,
      type: User.types.ADVISOR,
      phone: crmAgendData.telefono,
      email: crmAgendData.email,
      advisorId: crmAgendData.id,
      supportedUsers: crmAgendData.supportedUsers
    };
  },
  transformSurvey(surveyData) {
    const dataNascita = new Date(surveyData.birthDate1)
      .toISOString()
      .substring(0, 10);
    return {
      leadId: surveyData.lead_id,
      type: User.types.CUSTOMER,
      advisorId: surveyData.advisor_id,
      cityId: surveyData.comune,
      sex: surveyData.sesso,
      email: surveyData.email,
      firstName: surveyData.nome,
      lastName: surveyData.cognome,
      birthDate: dataNascita,
      age: hashUtil.computeAge(surveyData.birthDate1),
      exported: surveyData.exported
    };
  },
  notify: async (message, userId = null) => {
    var users = await redis.getUsers();
    const user = users.find((u) => u.userId == userId);
    if (user) {
      global.io
        .to(user.socketId)
        .emit("notification", { message: message, type: "standard" });
    }
  }
};
