import express from "express";
import { validate } from "express-validation";
import controller from "./user.controller.js";
import validation from "./user.validation.js";
import { image } from "../../middlewares/upload.js";
import role from "../../middlewares/role.js";
const router = express.Router();

router.param("id", controller.onLoad);

/**
 * @api {get} /v1/users/me                      me
 * @apiName                                     UserMe
 * @apiDescription                              Get my data, my room and my advisor rating.
 * @apiGroup                                    Users
 * @apiVersion                                  0.1.0
 * @apiPermission                               authenticated
 * @apiUse authenticated
 *
 * @apiSuccess  {Boolean}                             success                      Valid token?
 * @apiSuccess  {[User](#api-Models-ObjectUser)}      user                         The requested user.
 * @apiSuccess  {Object}                              room                         Room where it belongs.
 * @apiSuccess  {Object}                              rating                       Rating recap.
 *
 */
router.get("/me", controller.onMe);

/**
 * @api {get} /v1/users/enquire                 enquire
 * @apiName                                     UserEnquire
 * @apiDescription                              Get data from CRM
 * @apiGroup                                    Users
 * @apiVersion                                  0.1.0
 * @apiPermission                               authenticated
 * @apiUse authenticated
 *
 */
router.get("/enquire", controller.onEnquire);

/**
 * @api {get} /v1/users/sync                    sync
 * @apiName                                     SyncAdvisor
 * @apiDescription                              Synch my data from CRM. Only for advisor
 * @apiGroup                                    Users
 * @apiVersion                                  0.1.0
 * @apiPermission                               authenticated
 * @apiUse authenticated
 *
 * @apiSuccess  {Boolean}                             success                      Valid token?
 * @apiSuccess  {[User](#api-Models-ObjectUser)}      user                         The sync user.
 *
 */
router.get("/sync", role.isAdvisorOrAdmin, controller.onSync);

/**
 * @api {get} /v1/users/:advisorId/sync         sync adviosor data, room and its users data
 * @apiName                                     SyncFullAdvisor
 * @apiDescription                              Synch advisor rooom data and its users from CRM. Only for admin
 * @apiGroup                                    Users
 * @apiVersion                                  0.1.0
 * @apiPermission                               admin
 * @apiUse admin
 *
 * @apiSuccess  {Boolean}                             success                      Valid token?
 * @apiSuccess  {[User](#api-Models-ObjectUser)}      user                         The sync user.
 *
 */
router.get("/:id/sync", role.isAdvisorOrAdmin, controller.onFullSync);

/**
 * @api {get} /v1/users                     get all users
 * @apiName                                 AllUsers
 * @apiDescription                          Get all users.
 * @apiGroup                                Users
 * @apiVersion                              0.1.0
 * @apiPermission                           admin
 * @apiUse admin
 *
 * @apiSuccess  {Boolean}                           success         Authorized?
 * @apiSuccess  {[User[]](#api-Models-ObjectUser)}  users           List of users.
 *
 */
router.get(
  "/",
  [role.isAdvisorOrAdmin, validate(validation.search)],
  controller.onList
);

/**
 * @api {post} /v1/users                        create user
 * @apiName                                     CreateUser
 * @apiDescription                              Create user.
 * @apiGroup                                    Users
 * @apiVersion                                  0.1.0
 * @apiPermission                               admin
 * @apiUse admin
 *
 * @apiBody {String}                                            firstName          First name.
 * @apiBody {String}                                            lastName           Last name.
 * @apiBody {String}                                            phone              Phone number.
 * @apiBody {String}                                            email              Email address.
 * @apiBody {String="admin", "advisor", "editor", "customer"}   type               User type.
 * @apiBody {String}                                            [image=" "]        User image name.
 * @apiBody {Number}                                            [advisorId]        Advisor CRM ID. for **customer type** is its advisor, for **advisor type** is itself.
 * @apiBody {Number}                                            [leadId]           Lead CRM ID [only for customer type].
 * @apiBody {Number}                                            [supportedUsers]   How many user are supported from advisorId [only for advisor type].
 * @apiBody {String}                                            [fcmToken]         Firebase user device token [only for customer type].
 * @apiBody {Boolean}                                           [otpVerified]      Has it already been OTP verified.
 * @apiBody {Number}                                            [sex]              User gender [only for customer type].
 * @apiBody {Number}                                            [age]              User age [only for customer type].
 *
 * @apiSuccess  {Boolean}                           success         Authorized?
 * @apiSuccess  {[User](#api-Models-ObjectUser)}    user            User profile.
 *
 */
router.post(
  "/",
  [role.isAdmin, validate(validation.user)],
  controller.onCreate
);

/**
 * @api {get} /v1/users/:id                 get user
 * @apiName                                 GetUser
 * @apiDescription                          Get one user.
 * @apiGroup                                Users
 * @apiVersion                              0.1.0
 * @apiPermission                           admin
 * @apiUse admin
 *
 * @apiQuery    {String}                            id          User ID.
 *
 * @apiSuccess  {Boolean}                           success     Authorized?
 * @apiSuccess  {[User](#api-Models-ObjectUser)}    user        User profile.
 *
 */
router.get("/:id", role.isAdvisorOrAdmin, controller.onGet);

/**
 * @api {put} /v1/users/:id                 change user
 * @apiName                                 ChangeUser
 * @apiDescription                          Change one user.
 * @apiGroup                                Users
 * @apiVersion                              0.1.0
 * @apiPermission                           authenticated but user can only update himself. admin can update every user
 * @apiUse admin
 *
 * @apiQuery   {String}                                            id      User ID.
 *
 * @apiBody    {String}                                            email               Email address.
 * @apiBody    {String}                                            firstName           First name.
 * @apiBody    {String}                                            lastName            Last name.
 * @apiBody    {String="admin", "advisor", "editor", "customer"}   type                User type.
 * @apiBody    {file}                                              [image]             Profile image file.
 * @apiBody    {String}                                            [advisorId]         Advisor CRM ID. [for **customer type** is its advisor - for **advisor type** is itself].
 * @apiBody    {String}                                            [leadId]            Lead CRM ID [only for customer type].
 * @apiBody    {Boolean}                                           [otpVerified]       Has it already been OTP verified.
 * @apiBody    {String}                                            [fcmToken]          Firebase user device token [only for customer type].
 * @apiBody    {Number}                                            [supportedUsers]    How many user are supported from advisorId [only for advisor type].
 * @apiBody    {String}                                            [sex]               User gender [only for customer type].
 * @apiBody    {Number}                                            [age]               User age [only for customer type].
 *
 * @apiSuccess  {Boolean}                                           success     Authorized?
 * @apiSuccess  {[User](#api-Models-ObjectUser)}                    user        User profile.
 *
 */
router.put("/:id", image.single("image"), controller.onUpdate);

/**
 * @api {get} /v1/users/:id/rooms           get advisor rooms
 * @apiName                                 GetAdvisorRooms
 * @apiDescription                          Get advisor rooms user.
 * @apiGroup                                Users
 * @apiVersion                              0.1.0
 * @apiPermission                           admin
 * @apiUse admin
 *
 * @apiQuery    {String}                            id          User ID.
 *
 * @apiSuccess  {Boolean}                           success     Authorized?
 * @apiSuccess  {[User](#api-Models-ObjectUser)}    rooms       List of rooms.
 *
 */
router.get("/:id/rooms", role.isAdvisorOrAdmin, controller.onGetRooms);

/**
 * @api {get} /v1/users/redeem              add referred user
 * @apiName                                 AddReferredUser
 * @apiDescription                          Ad Referred User.
 * @apiGroup                                Users
 * @apiVersion                              0.1.0
 * @apiPermission                           admin
 * @apiUse admin
 *
 */
router.post("/redeem", role.isAdvisorOrAdmin, controller.onRedeem);

/**
 * @api {get} /v1/users/:id/notify          advisor notification
 * @apiName                                 PushNotificationAdvisor
 * @apiDescription                          Notify Adviosr on Panel.
 * @apiGroup                                Users
 * @apiVersion                              0.1.0
 * @apiPermission                           admin
 * @apiUse admin
 *
 */
router.post("/:id/notify", role.isAdmin, controller.onNotify);

export default router;
