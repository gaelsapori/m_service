import express from "express";
import { validate } from "express-validation";
import controller from "./rating.controller.js";
import validation from "./rating.validation.js";
import role from "../../middlewares/role.js";

const router = express.Router();

/**
 * @api {post} /v1/ratings            create rating
 * @apiName                           CreateRating
 * @apiDescription                    Create advisor rating.
 * @apiGroup                          Ratings
 * @apiVersion                        0.1.0
 * @apiPermission                     authenticated
 * @apiUse authenticated
 *
 * @apiBody {String}           advisorId        Advisor Id.
 * @apiBody {Number}           rate             Rating star.
 * @apiBody {String}           comment          Comment about advisor.
 *
 *
 * @apiSuccess  {Boolean}                           success             Authorized?
 * @apiSuccess  {[Post](#api-Models-ObjectPost)}    myRate              User rate recap object.
 * @apiSuccess  {String}                            myRate.userId       User Id.
 * @apiSuccess  {String}                            myRate.advisorId    Advisor Id.
 * @apiSuccess  {Number}                            myRate.rate         Rating star.
 * @apiSuccess  {String}                            myRate.comment      Comment.
 * @apiSuccess  {String}                            myRate._id          Rating Id.
 * @apiSuccess  {Object}                            rating              Rating recap.
 * @apiSuccess  {Number}                            rating.avg          Average rating.
 * @apiSuccess  {Number}                            rating.min          Minimum rating recieved.
 * @apiSuccess  {Number}                            rating.max          Maximium rating recieved.
 * @apiSuccess  {Number}                            rating.count        Count of vote recieved.
 * @apiSuccess  {String}                            rating.advisorId    Advisor ID.
 * @apiSuccess  {Boolean}                           rating.hasVoted     Did the user vote already the advisor?.
 *
 */
router.post("/", validate(validation.rating), controller.onCreate);

/**
 * @api {get} /v1/ratings             get advisor rating
 * @apiName                           GetAdvisorRating
 * @apiDescription                    Get my advisor rating.
 * @apiGroup                          Ratings
 * @apiVersion                        0.1.0
 * @apiPermission                     authenticated
 * @apiUse authenticated
 *
 * @apiSuccess  {Boolean}                           success             Authorized?
 * @apiSuccess  {Object}                            rating              Rating recap.
 * @apiSuccess  {Number}                            rating.avg          Average rating.
 * @apiSuccess  {Number}                            rating.min          Minimum rating recieved.
 * @apiSuccess  {Number}                            rating.max          Maximium rating recieved.
 * @apiSuccess  {Number}                            rating.count        Count of vote recieved.
 * @apiSuccess  {String}                            rating.advisorId    Advisor ID.
 * @apiSuccess  {Boolean}                           rating.hasVoted     Did the user vote already the advisor?.
 *
 */
router.get("/", controller.onGetMyAdvisorRating);

/**
 * @api {post} /v1/ratings            add comment
 * @apiName                           AddComment
 * @apiDescription                    Add comment to existing rating.
 * @apiGroup                          Ratings
 * @apiVersion                        0.1.0
 * @apiPermission                     authenticated
 * @apiUse authenticated
 *
 * @apiBody {String}           comment          Comment about advisor.
 *
 * @apiSuccess  {Boolean}                           success             Authorized?
 * @apiSuccess  {Object}                            rating              Rating recap.
 * @apiSuccess  {Number}                            rating.avg          Average rating.
 * @apiSuccess  {Number}                            rating.min          Minimum rating recieved.
 * @apiSuccess  {Number}                            rating.max          Maximium rating recieved.
 * @apiSuccess  {Number}                            rating.count        Count of vote recieved.
 * @apiSuccess  {String}                            rating.advisorId    Advisor ID.
 * @apiSuccess  {Boolean}                           rating.hasVoted     Did the user vote already the advisor?.
 */
router.put("/:ratingId", validate(validation.comment), controller.onComment);

/**
 * @api {post} /v1/ratings/all        get all rating
 * @apiName                           GetAllRating
 * @apiDescription                    Get all rating.
 * @apiGroup                          Ratings
 * @apiVersion                        0.1.0
 * @apiPermission                     admin
 * @apiUse admin
 */
router.get("/all", role.isAdmin, controller.onList);

/**
 * @api {post} /v1/ratings/:advisorId get adviosr rating
 * @apiName                           GetAdvisorRating
 * @apiDescription                    Get advisor rating.
 * @apiGroup                          Ratings
 * @apiVersion                        0.1.0
 * @apiPermission                     authenticated
 * @apiUse authenticated
 */
router.get("/:advisorId", controller.onListAdvisor);

export default router;
