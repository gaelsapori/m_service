import mongoose from "mongoose";
import { v4 as uuidv4 } from "uuid";

/**
 * @api {OBJECT} Rating Rating
 * @apiGroup Models
 * @apiVersion 0.1.0
 * @apiParam {String}     _id           Rating ID.
 * @apiParam {String}     advisorId     Advisor ID rated.
 * @apiParam {String}     userId        User ID who rate advisor.
 * @apiParam {Number}     rate          Rate recieved.
 * @apiParam {String}     [comment]       Comment recieved.
 * @apiParam {String}     createdAt     Creation date in ISO string.
 * @apiParam {String}     updatedAt     Update date in ISO string.
 */

const ratingSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4().replace(/-/g, "")
    },
    userId: { type: String, ref: "User", required: true },
    advisorId: { type: String, ref: "User", required: true },
    rate: { type: Number, required: true },
    comment: { type: String }
  },

  {
    timestamps: true,
    collection: "ratings"
  }
);

ratingSchema.statics = {
  async get(advisorId, condition) {
    const aggregate = await this.aggregate([
      { $match: { advisorId: advisorId } },
      {
        $project: {
          _id: 0,
          vote: {
            $cond: [{ $eq: condition }, 1, 0]
          },
          rate: 1
        }
      },
      {
        $group: {
          _id: advisorId,
          avg: { $avg: "$rate" },
          min: { $min: "$rate" },
          max: { $max: "$rate" },
          count: { $sum: 1 },
          hasVoted: { $sum: "$vote" }
        }
      },
      {
        $project: {
          _id: 0,
          advisorId: advisorId,
          avg: 1,
          min: 1,
          max: 1,
          count: 1,
          hasVoted: {
            $cond: [{ $gt: ["$hasVoted", 0] }, true, false]
          }
        }
      }
    ]);
    return aggregate[0];
  },
  async list({ page = 0, limit = 10, advisorId = "" }) {
    const conditions =
      advisorId != "" ? { advisorId: advisorId } : { advisorId: { $ne: "" } };

    const aggregate = await this.aggregate([
      { $match: conditions },
      {
        $lookup: {
          from: "users",
          localField: "userId",
          foreignField: "_id",
          as: "user"
        }
      },
      { $unwind: "$user" },
      {
        $lookup: {
          from: "users",
          localField: "advisorId",
          foreignField: "_id",
          as: "advisor"
        }
      },
      { $unwind: "$advisor" },
      {
        $project: {
          _id: 1,
          rate: 1,
          updatedAt: 1,
          comment: 1,
          user: {
            _id: 1,
            firstName: 1,
            lastName: 1,
            leadId: 1
          },
          advisor: {
            _id: 1,
            firstName: 1,
            lastName: 1
          }
        }
      },
      {
        $facet: {
          data: [
            { $sort: { updatedAt: -1 } },
            { $skip: page * limit },
            { $limit: limit }
          ],
          total: [{ $count: "total" }]
        }
      }
    ]);

    return aggregate[0];
  }
};

export default mongoose.model("Rating", ratingSchema);
