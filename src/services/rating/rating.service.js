import Rating from "./rating.model.js";
import httpStatus from "http-status";
import APIError from "../../utils/api.error.js";

export default {
  create: async (ratingData) => {
    return Rating.create(ratingData);
  },
  get: async (advisorId, condition) => {
    return await Rating.get(advisorId, condition);
  },
  list: async (params) => {
    const ratings = await Rating.list(params);
    return {
      total: ratings.total[0] ? ratings.total[0].total : 0,
      comments: ratings.data
    };
  },
  find: async (filter) => {
    const rating = await Rating.findOne(filter);
    if (!rating)
      throw new APIError({
        message: "Nessun rating trovato.",
        status: httpStatus.NOT_FOUND
      });
    return rating;
  },
  comment: async (id, comment) => {
    return await Rating.findOneAndUpdate(
      { _id: id },
      { comment },
      { upsert: false, new: true }
    );
  }
};
