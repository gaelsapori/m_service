import Joi from "joi";

export default {
  rating: {
    body: Joi.object({
      advisorId: Joi.string().required(),
      rate: Joi.number().required(),
      comment: Joi.string().allow("")
    })
  },
  comment: {
    body: Joi.object({
      comment: Joi.string().required()
    })
  }
};
