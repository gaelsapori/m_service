import service from "./rating.service.js";
import userService from "../user/user.service.js";
import { handler as errorHandler } from "../../middlewares/error.js";

export default {
  onCreate: async (req, res) => {
    try {
      const { advisorId, rate } = req.body;
      await userService.get(advisorId);
      await userService.get(req.userId);

      const r = { userId: req.userId, advisorId, rate, comment: "" };
      const vote = await service.create(r);
      const rating = await service.get(advisorId, ["$userId", req.userId]);

      return res.json({
        success: true,
        myRate: vote,
        rating: rating ? rating : null
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onGetMyAdvisorRating: async (req, res) => {
    try {
      const user = await userService.get(req.userId);
      const advisor = await userService.findOneAdvisor(user.advisorId, true);
      const rating = await service.get(advisor._id, ["$userId", user._id]);
      return res.json({ success: true, rating: rating ? rating : null });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onComment: async (req, res) => {
    try {
      const ratingId = req.params.ratingId;
      await service.find({ userId: req.userId, _id: ratingId });
      const rating = await service.comment(ratingId, req.body.comment);
      return res.json({ success: rating != null, rating });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onListAdvisor: async (req, res) => {
    try {
      const advisorId = req.params.advisorId;
      const options = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10,
        advisorId: advisorId
      };
      const rating = await service.get(advisorId, ["$advisorId", advisorId]);
      const list = await service.list(options);
      return res.json({
        success: true,
        rating: rating ? rating : null,
        ...list
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onList: async (req, res) => {
    try {
      const options = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10
      };
      const list = await service.list(options);
      return res.json({
        success: true,
        ...list
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
