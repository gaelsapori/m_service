import express from "express";
import controller from "./question.controller.js";

const router = express.Router();

/**
 * @api {get} /v1/questions/:section            get questions
 * @apiName                                     GetQuestions
 * @apiDescription                              Get questions.
 * @apiGroup                                    Questions
 * @apiVersion                                  0.1.0
 * @apiPermission                               authenticated
 * @apiUse authenticated
 *
 * @apiQuery    {String}                                    section             Section tab.
 *
 * @apiSuccess  {Boolean}                                   success             Authorized?
 * @apiSuccess  {[Question](#api-Models-ObjectQuestion)}    questions           List of questions.
 *
 */
router.get("/:section", controller.onGetQestions);

export default router;
