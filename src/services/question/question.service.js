import Question from "./question.model.js";

export default {
  get: async (section) => {
    return await Question.find({ section: section });
  }
};
