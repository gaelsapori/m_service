import service from "./question.service.js";
import { handler as errorHandler } from "../../middlewares/error.js";

export default {
  onGetQestions: async (req, res) => {
    try {
      const questions = await service.get(req.params.section);
      return res.json({ success: true, questions });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
