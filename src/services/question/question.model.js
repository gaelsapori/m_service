import mongoose from "mongoose";
import { v4 as uuidv4 } from "uuid";

const optionSchema = new mongoose.Schema(
  {
    _id: false,
    label: String,
    value: String
  },
  {
    timestamps: false
  }
);

const questionSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4().replace(/-/g, "")
    },
    section: String,
    sys_name: String,
    question: String,
    name: String,
    placeholder: String,
    options: [optionSchema]
  },
  {
    timestamps: true,
    collection: "questions"
  }
);

export default mongoose.model("Question", questionSchema);

/**
 * @api {OBJECT} Question Question
 * @apiGroup Models
 * @apiVersion 0.1.0
 * @apiParam  {String}                            question._id                    Question id.
 * @apiParam  {String}                            question.section                Section tab.
 * @apiParam  {String}                            question.sys_name               Question system name.
 * @apiParam  {String}                            question.question               Question sentence.
 * @apiParam  {String}                            question.name                   Question title.
 * @apiParam  {String}                            question.placeholder            Question placeholder for empty input.
 * @apiParam  {Object[]}                          [question.options]              List of options
 * @apiParam  {String}                            [question.options.label]        Option label.
 * @apiParam  {String}                            [question.options.value]        Option value.
 */
