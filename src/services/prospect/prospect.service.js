import Term from "./terms.model.js";

export default {
  transform: (prospects) => {
    if (prospects.length) {
      for (const p in prospects) {
        prospects[p].extra_spese = JSON.parse(prospects[p].extra_spese);
        prospects[p].isNew = true;
        prospects[p].tan = parseFloat(prospects[p].tan);
        prospects[p].taeg = parseFloat(prospects[p].taeg);
        prospects[p].pagamento_mensile = parseFloat(
          prospects[p].pagamento_mensile
        );
        prospects[p].reddito_mensile_netto = parseFloat(
          prospects[p].reddito_mensile_netto
        );
        prospects[p].prezzo_immobile = parseFloat(prospects[p].prezzo_immobile);
        prospects[p].valore_mutuo = parseFloat(prospects[p].valore_mutuo);
      }
    }
    return prospects;
  },
  getTerms: async () => {
    return await Term.findTerms();
  }
};
