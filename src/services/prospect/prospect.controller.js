import CrmLookup from "../crm/crm.lookup.js";
import service from "./prospect.service.js";
import userService from "../user/user.service.js";
import { handler as errorHandler } from "../../middlewares/error.js";

export default {
  onList: async (req, res) => {
    try {
      const user = await userService.get(req.userId);
      const crmProspect = await CrmLookup.myProspects(user.leadId);
      const prospects = service.transform(crmProspect);
      return res.json({ success: true, prospects });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onGetTerms: async (req, res) => {
    try {
      const terms = await service.getTerms();
      return res.json({ success: true, terms });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
