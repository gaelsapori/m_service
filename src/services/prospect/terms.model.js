import mongoose from "mongoose";
import { v4 as uuidv4 } from "uuid";
const termSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4().replace(/-/g, "")
    },
    name: {
      type: String
    },
    title: {
      type: String
    },
    url: {
      type: String
    }
  },
  {
    timestamps: true,
    collection: "terms"
  }
);
termSchema.statics.findTerms = async function () {
  const terms = await this.find();
  return terms;
};

export default mongoose.model("Term", termSchema);
