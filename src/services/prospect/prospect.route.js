import express from "express";
import controller from "./prospect.controller.js";

const router = express.Router();

/**
 * @api {get} /v1/prospects           get prospects
 * @apiName                           GetProspects
 * @apiDescription                    Get user mortgage prospects from CRM with a valid creation date.
 * @apiGroup                          Prospects
 * @apiVersion                        0.1.0
 * @apiPermission                     authenticated
 * @apiUse authenticated
 *
 * @apiSuccess  {Boolean}                                       success             Authorized?
 * @apiSuccess  {[Prospect[]](#api-Models-ObjectProspect)}      prospects           List of prospects.
 *
 */
router.get("/", controller.onList);

/**
 * @api {get} /v1/prospects/terms           get all terms
 * @apiName                                 GetTerms
 * @apiDescription                          Get all terms and conditions.
 * @apiGroup                                Prospects
 * @apiVersion                              0.1.0
 * @apiPermission                           authenticated
 * @apiUse authenticated
 *
 * @apiSuccess  {Boolean}       success         Authorized?
 * @apiSuccess  {Object[]}      terms           List of terms.
 * @apiSuccess  {String}        terms.name      Bank code.
 * @apiSuccess  {String}        terms.title     Terms title.
 * @apiSuccess  {String}        terms.url       Terms url.
 *
 */
router.get("/terms", controller.onGetTerms);

export default router;
