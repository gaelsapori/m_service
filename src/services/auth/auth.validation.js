import Joi from "joi";

export default {
  email: {
    body: Joi.object({ email: Joi.string().email().required() })
  }
};
