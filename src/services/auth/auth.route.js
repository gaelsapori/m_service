import express from "express";
import { validate } from "express-validation";
import { encode, apikey } from "../../middlewares/jwt.js";
import controller from "./auth.controller.js";
import validation from "./auth.validation.js";

const router = express.Router();

/**
 * @api {post} /v1/auth/email                   email authentication
 * @apiName                                     EmailAuth
 * @apiDescription                              Authenticate advisor by its email.
 *
 * An email is sent with a tokenized link.
 * @apiGroup                                    Authentication
 * @apiVersion                                  0.1.0
 * @apiPermission                               none
 *
 * @apiParam    {String}    email           Advisor email address.
 *
 * @apiSuccess  {Boolean}   success         Email message has been sent?
 * @apiSuccess  {String}    message         Response message.
 *
 */

router.post("/email", validate(validation.email), controller.onEmailLogin);

/**
 * @api {get} /v1/auth/token/:token             token authentication
 * @apiName                                     TokenAuth
 * @apiDescription                              Authenticate advisor with its token.
 * @apiGroup                                    Authentication
 * @apiVersion                                  0.1.0
 * @apiPermission                               none
 *
 * @apiParam    {String}    token                       User token.
 *
 * @apiSuccess  {Boolean}   success                     Valid token?
 * @apiSuccess  {String}    token                       Token.
 * @apiSuccess  {[User](#api-Models-ObjectUser)} user   The requested user.
 *
 */
router.get("/token/:token", controller.onTokenLogin);

/**
 * @api {post} /v1/auth/userid/:userId        token from id
 * @apiName                                   TokenFromId
 * @apiGroup                                  Authentication
 * @apiVersion                                0.1.0
 * @apiPermission                             apikey
 * @apiDescription                            Authenticate user from its ID.
 *
 * @apiHeader       {String}    x-api-key   Unique api-key.
 *
 * @apiParam        {String}    userId      User ID.
 *
 * @apiSuccess      {Boolean}   success     User exists?
 * @apiSuccess      {String}    token       Token.
 *
 */
router.post("/userid/:userId", [apikey, encode], controller.onUserIdLogin);

export default router;
