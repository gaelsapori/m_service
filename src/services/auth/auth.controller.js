import userService from "../user/user.service.js";
import envar from "../../config/vars.js";
import mailer from "../../utils/mailer.js";
import jwt from "jsonwebtoken";

export default {
  onEmailLogin: async (req, res, next) => {
    try {
      const { email } = req.body;
      const user = await userService.findAdvisorEmail(email);
      const token = user.token("300s");
      await mailer.sendVerificationEmail(user, token);

      return res.json({
        success: true,
        message: "Email inviata correttamente"
      });
    } catch (error) {
      next(error);
    }
  },
  onTokenLogin: async (req, res, next) => {
    try {
      const { token } = req.params;
      const decoded = jwt.verify(token, envar.jwtSecret);
      const user = await userService.get(decoded.userId);
      const authToken = user.token();

      return res.json({ success: true, token: authToken, user });
    } catch (error) {
      next(error);
    }
  },
  onUserIdLogin: async (req, res, next) => {
    try {
      return res.json({ success: true, token: req.authToken });
    } catch (error) {
      next(error);
    }
  }
};
