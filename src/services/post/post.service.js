import Post from "./post.model.js";
import httpStatus from "http-status";
import APIError from "../../utils/api.error.js";

export default {
  list: async (params) => {
    const posts = await Post.list(params);
    return {
      total: posts.total[0] ? posts.total[0].total : 0,
      posts: posts.data
    };
  },
  delete: async (id) => {
    return await Post.deleteOne({ _id: id });
  },
  create: async (post) => {
    return await Post.create(post);
  },
  get: async (id) => {
    return await Post.findById(id);
  },
  update: async (id, body) => {
    if (body.status != null) {
      body.status = body.status == "true" ? true : false;
    }
    const post = await Post.findOne({ id: id });
    body.publicationDate = new Date(body.publicationDate).toISOString();
    if (!post)
      throw new APIError({
        message: "Nessun articolo trovato.",
        status: httpStatus.NOT_FOUND
      });
    const updatedPost = await Post.findOneAndUpdate({ _id: id }, body, {
      upsert: false,
      new: true,
      timestamps: true
    });
    return updatedPost;
  }
};
