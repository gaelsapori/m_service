import Joi from "joi";

export default {
  post: {
    body: Joi.object({
      _id: Joi.string(),
      title: Joi.string().required(),
      body: Joi.string().required(),
      excerpt: Joi.string().required(),
      status: Joi.string(),
      cta: Joi.string().allow(null, ""),
      image: Joi.string().allow(null, ""),
      target: Joi.string().allow(null, ""),
      publicationDate: Joi.date().iso()
    })
  }
};
