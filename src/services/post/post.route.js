import express from "express";
import { validate } from "express-validation";
import controller from "./post.controller.js";
import validation from "./post.validation.js";
import { image } from "../../middlewares/upload.js";
import role from "../../middlewares/role.js";

const router = express.Router();

/**
 * @api {get} /v1/posts?limit=:limit&page=:page     get all posts
 * @apiName                                         AllPosts
 * @apiDescription                                  Get all posts.
 * @apiGroup                                        Posts
 * @apiVersion                                      0.1.0
 * @apiPermission                                   authenticated
 * @apiUse authenticated
 *
 * @apiQuery {Number} limit=5 Maximum posts
 * @apiQuery {Number} page=0 Page index (zero based)
 *
 * @apiSuccess  {Boolean}                           success         Authorized?
 * @apiSuccess  {[Post[]](#api-Models-ObjectPost)}  posts           List of posts.
 * @apiSuccess  {Number}                            total           Count of total posts.
 *
 */
router.get("/", controller.onList);

/**
 * @api {post} /v1/posts              create post
 * @apiName                           CreatePost
 * @apiDescription                    Create a post via form-data.
 * @apiGroup                          Posts
 * @apiVersion                        0.1.0
 * @apiPermission                     admin and editor
 * @apiUse admin
 *
 * @apiBody {String}                                                 title               Post title.
 * @apiBody {String}                                                 excerpt             Post excerpt.
 * @apiBody {String}                                                 body                Post body.
 * @apiBody {File}                                                   [image]             Cover image.
 * @apiBody {Boolean}                                                [status]            Post published?
 * @apiBody {String}                                                 [cta]               Call to action text.
 * @apiBody {String="home", "prospetti", "chat", "tassi", "agenda"}  [target]            Call to action target
 * @apiBody {String}                                                 [publicationDate]   Publication date in ISO string.
 *
 * @apiSuccess  {Boolean}                           success         Authorized?
 * @apiSuccess  {[Post](#api-Models-ObjectPost)}    post            Post object.
 *
 */
router.post(
  "/",
  [role.isEditor, image.single("image"), validate(validation.post)],
  controller.onCreate
);

/**
 * @api {get} /v1/posts/:postId         get post
 * @apiName                             GetPost
 * @apiDescription                      Get one post.
 * @apiGroup                            Posts
 * @apiVersion                          0.1.0
 * @apiPermission                       authenticated
 * @apiUse authenticated
 *
 * @apiQuery    {String}                            postId      Post ID.
 *
 * @apiSuccess  {Boolean}                           success     Authorized?
 * @apiSuccess  {[Post](#api-Models-ObjectPost)}    user        Post object.
 *
 */
router.get("/:id", controller.onGet);

/**
 * @api {put} /v1/posts/:postId           change post
 * @apiName                               ChangePost
 * @apiDescription                        Change a post via form-data.
 * @apiGroup                              Posts
 * @apiVersion                            0.1.0
 * @apiPermission                         admin and editor
 * @apiUse admin
 * 
 * @apiQuery {String}                                                 postId              Post ID.
 *
 * @apiBody  {String}                                                 title               Post title.
 * @apiBody  {String}                                                 excerpt             Post excerpt.
 * @apiBody  {String}                                                 body                Post body.
 * @apiBody  {String}                                                 publicationDate     Publication date in ISO string.
 * @apiBody  {File}                                                   [image]             Cover image.
 * @apiBody  {Boolean}                                                [status]            Post published?
 * @apiBody  {String}                                                 [cta]               Call to action text.
 * @apiBody  {String="home", "prospetti", "chat", "tassi", "agenda"}  [target]            Call to action target

 *
 * @apiSuccess  {Boolean}                           success         Authorized?
 * @apiSuccess  {[Post](#api-Models-ObjectPost)}    post            Post object.
 *
 */
router.put(
  "/:id",
  [role.isEditor, image.single("image"), validate(validation.post)],
  controller.onUpdate
);

export default router;
