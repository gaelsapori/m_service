import mongoose from "mongoose";
import { v4 as uuidv4 } from "uuid";

const postSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4().replace(/-/g, "")
    },
    title: {
      type: String,
      required: true
    },
    excerpt: {
      type: String,
      required: true
    },
    body: {
      type: String,
      required: true
    },
    image: String,
    publicationDate: { type: Date, default: Date.now },
    cta: String,
    target: String,
    status: Boolean
  },
  {
    timestamps: true,
    collection: "posts"
  }
);

postSchema.statics = {
  async list({ page = 0, limit = 10, active = true }) {
    const conditions = active == true ? { status: true } : {};
    const aggregate = await this.aggregate([
      { $match: conditions },
      {
        $facet: {
          data: [
            { $sort: { publicationDate: -1 } },
            { $skip: page * limit },
            { $limit: limit }
          ],
          total: [{ $count: "total" }]
        }
      }
    ]);

    return aggregate[0];
  }
};

export default mongoose.model("Post", postSchema);

/**
 * @api {OBJECT} Post Post
 * @apiGroup Models
 * @apiVersion 0.1.0
 * @apiParam {String}                                                 _id                 Post ID.
 * @apiParam {String}                                                 title               Post title.
 * @apiParam {String}                                                 excerpt             Post excerpt.
 * @apiParam {String}                                                 body                Post body.
 * @apiParam {String}                                                 image               Cover image.
 * @apiParam {Boolean}                                                status              Post published?
 * @apiParam {String}                                                 [cta]               Call to action text.
 * @apiParam {String="home", "prospetti", "chat", "tassi", "agenda"}  [target]            Call to action target
 * @apiParam {String}                                                 publicationDate     Publication date in ISO string.
 * @apiParam {String}                                                 createdAt           Creation date in ISO string.
 * @apiParam {String}                                                 updatedAt           Update date in ISO string.
 *
 */
