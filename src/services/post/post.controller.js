import httpStatus from "http-status";
import service from "./post.service.js";
import { handler as errorHandler } from "../../middlewares/error.js";
import APIError from "../../utils/api.error.js";

export default {
  onList: async (req, res) => {
    try {
      const options = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 5,
        active: req.query.active === "true" ? true : false
      };
      const response = await service.list(options);
      return res.json({ success: true, ...response });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onCreate: async (req, res) => {
    try {
      req.body.image = req.file ? req.file.filename : "";
      const post = await service.create(req.body);
      return res.json({ success: true, post });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onGet: async (req, res) => {
    try {
      const post = await service.get(req.params.id);
      if (!post) {
        throw new APIError({
          message: "L'articolo non esiste",
          status: httpStatus.NOT_FOUND
        });
      }
      return res.json({ success: true, post });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onUpdate: async (req, res) => {
    try {
      const exists = await service.get(req.params.id);
      if (!exists) {
        throw new APIError({
          message: "L'articolo non esiste",
          status: httpStatus.NOT_FOUND
        });
      }
      if (req.file) req.body.image = req.file.filename;

      const post = await service.update(req.params.id, req.body);
      return res.json({ success: true, post });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onDelete: async (req, res) => {
    try {
      const post = await service.delete(req.params.id);
      return res.json({
        success: true,
        message: `Deleted a count of ${post.deletedCount} post.`
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
