import mongoose from "mongoose";
import { v4 as uuidv4 } from "uuid";
const topicSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4().replace(/-/g, "")
    },
    name: {
      unique: true,
      type: String
    }
  },
  {
    timestamps: true,
    collection: "topics"
  }
);

topicSchema.statics.createTopic = async function (name) {
  const topic = await this.create({ name });
  return topic;
};

export default mongoose.model("Topic", topicSchema);
