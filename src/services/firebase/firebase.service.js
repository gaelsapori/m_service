import Topic from "./topic.model.js";
import httpStatus from "http-status";
import fcm from "firebase-admin";
import { readFile } from "fs/promises";
import APIError from "../../utils/api.error.js";

fcm.initializeApp({
  credential: fcm.credential.cert(
    JSON.parse(
      await readFile(
        new URL("../../certificates/firebase.json", import.meta.url)
      )
    )
  )
});

const notification_options = {
  priority: "high",
  content_available: true,
  timeToLive: 60 * 60 * 24
};

export default {
  listTopics: async () => {
    return await Topic.find();
  },
  createTopic: async (name) => {
    return await Topic.create({ name });
  },
  message: async ({ title, body, data, token = null, topic = null }) => {
    if (token == null && topic == null) {
      throw new APIError({
        message: "Messaggio senza destinatario.",
        status: httpStatus.BAD_REQUEST
      });
    }
    const options = notification_options;
    const message = {
      notification: {
        title,
        body,
        icon: "mutuiamo_icon",
        sound: "default",
        click_action: "FLUTTER_NOTIFICATION_CLICK"
      },
      data: { ...data }
    };
    if (token != null)
      await fcm.messaging().sendToDevice(token, message, options);
    if (topic != null)
      await fcm.messaging().sendToTopic(topic, message, options);
  }
};
