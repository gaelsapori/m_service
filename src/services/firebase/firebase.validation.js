import Joi from "joi";

export default {
  message: {
    body: Joi.object({
      title: Joi.string().required(),
      body: Joi.string().required(),
      data: Joi.object().required(),
      topic: Joi.string(),
      token: Joi.string()
    })
  },
  topic: {
    body: Joi.object({
      name: Joi.string().required()
    })
  }
};
