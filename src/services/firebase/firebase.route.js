import express from "express";
import { validate } from "express-validation";
import controller from "./firebase.controller.js";
import validation from "./firebase.validation.js";
import role from "../../middlewares/role.js";
const router = express.Router();

/**
 * @api {post} /v1/firebase/device    push device
 * @apiName                           PushDevice
 * @apiDescription                    Send push notififcation to device.
 * @apiGroup                          Firebase
 * @apiVersion                        0.1.0
 * @apiPermission                     admin
 * @apiUse admin
 *
 * @apiBody {String}                                                             title                   Post title.
 * @apiBody {String}                                                             body                    Post body.
 * @apiBody {Object}                                                             data                    Push data object.
 * @apiBody {String}                                                             data[cta]               Call to action text.
 * @apiBody {String}                                                             data[postId]            Post Id if target is "articolo".
 * @apiBody {String="home", "prospetti", "chat", "tassi", "agenda", "articolo"}  data[target]            Call to action target.
 * @apiBody {String}                                                             token                   User device FCM Token.
 *
 * @apiSuccess  {Boolean}       success         Authorized?
 * @apiSuccess  {String}        message         Message response.
 *
 */
router.post(
  "/",
  [role.isAdmin, validate(validation.message)],
  controller.onPushNotification
);

/**
 * @api {get} /v1/firebase/topics     get all topics
 * @apiName                           GetTopics
 * @apiDescription                    Get firebase topics.
 * @apiGroup                          Firebase
 * @apiVersion                        0.1.0
 * @apiPermission                     admin
 * @apiUse admin
 *
 * @apiSuccess  {Boolean}       success             Authorized?
 * @apiSuccess  {Object[]}      topics              List of topics.
 * @apiSuccess  {String}        topics._id          Topic ID.
 * @apiSuccess  {String}        topics.name         Topic name.
 * @apiSuccess  {String}        topics.createdAt    Topic creation date in ISO string.
 * @apiSuccess  {String}        topics.updatedAt    Topic updated date in ISO string.
 *
 */
router.get("/topics", role.isAdmin, controller.onListTopics);

/**
 * @api {post} /v1/firebase/topic/new               create topic
 * @apiName                                         CreateTopic
 * @apiDescription                                  Create Firebase topic.
 * @apiGroup                                        Firebase
 * @apiVersion                                      0.1.0
 * @apiPermission                                   admin
 * @apiUse admin
 *
 * @apiBody  {String}        name                   Topic name.
 *
 * @apiSuccess  {Boolean}                           success         Authorized?
 * @apiSuccess  {[Post](#api-Models-ObjectPost)}    message         Message response.
 * @apiSuccess  {[Topic](#api-Models-ObjectTopic)}  topic           Topic object.
 *
 */
router.post(
  "/topics/new",
  [role.isAdmin, validate(validation.topic)],
  controller.onCreateTopic
);

export default router;
