import service from "./firebase.service.js";
import { handler as errorHandler } from "../../middlewares/error.js";
import httpStatus from "http-status";
import APIError from "../../utils/api.error.js";

export default {
  onPushNotification: async (req, res) => {
    try {
      await service.message(req.body);
      return res.json({
        success: true,
        message: "Notification inviata."
      });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onCreateTopic: async (req, res) => {
    try {
      const { name } = req.body;
      const topic = await service.createTopic(name);
      return res.json({ success: true, message: "Topic creato.", topic });
    } catch (error) {
      if (error.code === 11000)
        throw new APIError({
          message: "Il topic già esiste.",
          status: httpStatus.BAD_REQUEST
        });

      return errorHandler(error, req, res);
    }
  },
  onListTopics: async (req, res) => {
    try {
      const topics = await service.listTopics();
      return res.json({ success: true, topics });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
