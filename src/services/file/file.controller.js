import mongoose from "mongoose";
import httpStatus from "http-status";
import APIError from "../../utils/api.error.js";
import { handler as errorHandler } from "../../middlewares/error.js";

const findInBucket = async (
  req,
  res,
  bucket,
  checkOwnership = false,
  fileTypes = ["image/jpeg", "image/png"]
) => {
  try {
    const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db, {
      bucketName: bucket
    });
    const file = await gfs.find({ filename: req.params.filename }).toArray();

    if (!file || file.length === 0) {
      throw new APIError({
        message: "Nessun file trovato.",
        status: httpStatus.NOT_FOUND
      });
    }

    if (checkOwnership === true) {
      const currentLoggedUser = req.userId;
      if (
        currentLoggedUser != file[0].metadata.userId &&
        req.userType == "customer"
      ) {
        throw new APIError({
          message: "Non sei autorizzato.",
          status: httpStatus.UNAUTHORIZED
        });
      }
    }

    if (fileTypes.includes(file[0].contentType)) {
      res.set("content-type", file[0].contentType);
      const readstream = gfs.openDownloadStreamByName(file[0].filename);
      return readstream.pipe(res);
    } else {
      throw new APIError({
        message: "Non è un file di tipo " + fileTypes.join(", "),
        status: httpStatus.NOT_FOUND
      });
    }
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

const deleteInBucket = async (req, res, bucket) => {
  try {
    const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db, {
      bucketName: bucket
    });
    const file = await gfs.find({ filename: req.params.filename }).toArray();
    if (!file || file.length === 0) {
      throw new APIError({
        message: "Nessun file trovato.",
        status: httpStatus.NOT_FOUND
      });
    }

    gfs.delete(file[0]._id, (err) => {
      return res.json({
        success: true,
        message: "File eliminato"
      });
    });
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

export default {
  onGetDocument: async (req, res) => {
    try {
      findInBucket(req, res, "documents", true);
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onGetImage: async (req, res) => {
    try {
      findInBucket(req, res, "images");
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onGetPdf: async (req, res) => {
    try {
      findInBucket(req, res, "documents", true, ["application/pdf"]);
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onDeleteImage: async (req, res) => {
    try {
      deleteInBucket(req, res, "images");
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onDeleteDocument: async (req, res) => {
    try {
      deleteInBucket(req, res, "documents");
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
