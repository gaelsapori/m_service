import express from "express";
import controller from "./file.controller.js";

const router = express.Router();

/**
 * @api {get}  /v1/files/image/:imageName       display image
 * @apiName                                     ShowImage
 * @apiDescription                              Display image.
 * @apiGroup                                    Posts
 * @apiVersion                                  0.1.0
 * @apiPermission                               authenticated
 * @apiUse authenticated
 *
 * @apiParam    {String}        imageName             Image name.
 *
 */
router.get("/image/:filename", controller.onGetImage);

/**
 * @api {get} /v1/files/document/:docName       display document
 * @apiName                                     ShowDocument
 * @apiDescription                              Display document. Customer can see only their documents.
 * @apiGroup                                    Rooms
 * @apiVersion                                  0.1.0
 * @apiPermission                               authenticated and ownership (customer) 
 * @apiUse authenticated

 *
 * @apiParam    {String}        docName               Image name.
 *
 */
router.get("/document/:filename", controller.onGetDocument);

/**
 * @api {get} /v1/files/pdf/:pdfName            display pdf
 * @apiName                                     ShowPDF
 * @apiDescription                              Display pdf. Customer can see only their pdf.
 * @apiGroup                                    Rooms
 * @apiVersion                                  0.1.0
 * @apiPermission                               authenticated and ownership (customer) 
 * @apiUse authenticated

 *
 * @apiParam    {String}        pdfName               PDF name.
 *
 */
router.get("/pdf/:filename", controller.onGetPdf);

export default router;
