import mongoose from "mongoose";
import { v4 as uuidv4 } from "uuid";

const rateSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4().replace(/-/g, "")
    },
    cityId: {
      type: Number,
      required: true
    },
    housePrice: {
      type: Number,
      required: true
    },
    mortgage: {
      type: Number,
      required: true
    },
    mortgageDuration: {
      type: Number,
      required: true
    },
    tan: {
      type: Number,
      required: true
    },
    taeg: {
      type: Number,
      required: true
    },
    payment: {
      type: Number,
      required: true
    },
    user: { type: String, ref: "User" },
    maxMontlyPayment: Number,
    maxLoanAmount: Number,
    maxHomeValue: Number,
    researchStage: String
  },
  {
    timestamps: true,
    collection: "rates"
  }
);

rateSchema.statics = {
  async getMyRates(userId) {
    return await this.aggregate([
      { $match: { user: userId } },
      {
        $lookup: {
          from: "users",
          localField: "user",
          foreignField: "_id",
          as: "user"
        }
      },
      { $unwind: "$user" },
      {
        $project: {
          _id: 1,
          cityId: 1,
          housePrice: 1,
          mortgage: 1,
          mortgageDuration: 1,
          tan: 1,
          taeg: 1,
          payment: 1,
          user: {
            _id: 1,
            firstName: 1,
            lastName: 1,
            phone: 1,
            email: 1
          },
          createdAt: 1,
          updatedAt: 1
        }
      }
    ]);
  }
};

export default mongoose.model("Rate", rateSchema);

/**
 * @api {OBJECT} Rate Rate
 * @apiGroup Models
 * @apiVersion 0.1.0
 * @apiParam  {String}                            rate._id                    Rate id.
 * @apiParam  {Number}                            rate.cityId                 City id.
 * @apiParam  {Number}                            rate.housePrice             House price.
 * @apiParam  {Number}                            rate.mortgage               Mortgage value.
 * @apiParam  {Number}                            rate.mortgageDuration       Mortgage duration.
 * @apiParam  {Number}                            rate.tan                    Mortage TAN.
 * @apiParam  {Number}                            rate.taeg                   Mortgage TAEG.
 * @apiParam  {Number}                            rate.payment                Monthly fee.
 * @apiParam  {[User](#api-Models-ObjectUser)}    rate.user                   User ID whom asked mortgage rate.
 */
