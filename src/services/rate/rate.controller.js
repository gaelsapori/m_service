import User from "../user/user.model.js";
import CrmLookup from "../crm/crm.lookup.js";
import service from "./rate.service.js";
import { handler as errorHandler } from "../../middlewares/error.js";

export default {
  onAskRate: async (req, res) => {
    try {
      const user = await service.getUser(req.userId, req.body);
      const loanValues = await service.askRates(
        req.body,
        req.query.borrowersBirthDates
      );
      const rate = await service.create(req.body, loanValues, user);
      return res.json({ success: true, rate });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onListByUser: async (req, res) => {
    try {
      const rates = await service.list(req.params.userId);
      return res.json({ success: true, rates });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  onGetLastRate: async (req, res) => {
    try {
      const user = await service.getUser(req.userId);
      const leadData = await CrmLookup.survey("id", user.leadId);
      let rate = await service.getMyLast(req.userId);
      if (rate == null && leadData != null) {
        const borrowers = service.borrowers(leadData);
        if (leadData.income2 == null) borrowers.splice(1, 1);
        const maxRate = service.maxRate(borrowers, leadData.nucleo_familiare);
        const mortgageDuration = service.mortgageDuration(borrowers);

        const loanValues = await service.askRates({
          cityId: leadData.comune,
          housePrice: leadData.prezzo_immobile,
          mortgage: leadData.valore_mutuo,
          mortgageDuration: mortgageDuration
        });

        user.researchStage = leadData.research_stage;
        User.updateUser(user);

        rate = await service.create(
          {
            cityId: leadData.comune,
            housePrice: leadData.prezzo_immobile,
            mortgage: leadData.valore_mutuo,
            mortgageDuration,
            user: user.id,
            maxMontlyPayment: maxRate
          },
          loanValues,
          user
        );
      }
      return res.json({ success: true, rate });
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
