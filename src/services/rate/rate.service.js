import axios from "axios";
import envar from "../../config/vars.js";
import User from "../user/user.model.js";
import Rate from "../rate/rate.model.js";
import httpStatus from "http-status";
import APIError from "../../utils/api.error.js";
import hashUtil from "../../utils/hash.js";
const debtToIncomeRatio = 0.33;
const defaultLoanTerm = 30;
const paymentsPerYear = 12;
const TAN = 0.025;
const maxLoanerAge = 75;
const familySustainabilityRatio = [1000, 1000, 1340, 1680, 2020, 2360];

const round5 = (x) => Math.floor(x / 5) * 5;

const getZValue = (borrowers) => {
  const olderBorrowerAge = Math.max.apply(
    Math,
    borrowers.map((b) => hashUtil.computeAge(b.birthDate))
  );
  const borrowerAgeDiff = maxLoanerAge - olderBorrowerAge;
  const loanTerm =
    borrowerAgeDiff >= defaultLoanTerm ? defaultLoanTerm : borrowerAgeDiff;
  return Math.pow(1 + TAN / paymentsPerYear, paymentsPerYear * loanTerm);
};

export default {
  askRates: async (body, borrowersBirthDates) => {
    let params = {};
    if (borrowersBirthDates) {
      params = { borrowersBirthDates, ...body };
    } else {
      params = body;
    }
    const request = await axios.get(envar.endpoint.rates, { params: params });
    if (!request.data.loanValues) {
      throw new APIError({
        message: "Quotazione non disponibile.",
        status: httpStatus.NOT_FOUND
      });
    }

    return request.data.loanValues;
  },
  list: (userId) => {
    return Rate.getMyRates(userId);
  },
  getMyLast: async (userId) => {
    return Rate.findOne({ user: userId }).sort({
      createdAt: -1
    });
  },
  getUser: async (id, body = {}) => {
    let update = false;
    let user = await User.get(id);
    if (body.city != "" && body.city != null) {
      const city = JSON.parse(body.city);
      user.city = city;
      user.cityId = city.idComune;
      update = true;
    }
    if (body.researchStage != "" && body.researchStage != null) {
      user.researchStage = body.researchStage;
      update = true;
    }
    if (update) User.updateUser(user);
    return user;
  },
  create: async (body, data, user) => {
    let rate = {
      ...data,
      ...body,
      user: user.id,
      researchStage: user.researchStage
    };

    Object.keys(rate).forEach((key) => {
      if (rate[key] === null || rate[key] === "null") {
        delete rate[key];
      }
    });
    return await Rate.create(rate);
  },
  borrowers: (leadData) => [
    {
      salaryNumber: leadData.salaryNumber1 ?? 13,
      income: leadData.income1,
      loanCost: leadData.loanCost1,
      birthDate: new Date(leadData.birthDate1).toISOString().substring(0, 10)
    },
    {
      salaryNumber: leadData.salaryNumber2 ?? 13,
      income: leadData.income2,
      loanCost: leadData.loanCost2,
      birthDate:
        leadData.birthDate2 != null
          ? new Date(leadData.birthDate2).toISOString().substring(0, 10)
          : null
    }
  ],
  maxRate: (borrowers, familySize) => {
    const size = parseInt(familySize);
    let totalMontlyLoanCost = 0.0;
    let totalMontlyIncome = 0.0;
    let maxMontlyPayment = 0.0;

    for (let i in borrowers) {
      let normalizedMontlyIncome =
        parseInt(borrowers[i].income) *
        (parseInt(borrowers[i].salaryNumber) / 12);
      totalMontlyLoanCost +=
        borrowers[i].loanCost != null ? parseInt(borrowers[i].loanCost) : 0.0;
      totalMontlyIncome += normalizedMontlyIncome;
      maxMontlyPayment += normalizedMontlyIncome * debtToIncomeRatio;
    }

    maxMontlyPayment -= totalMontlyLoanCost;
    const actualIncome =
      totalMontlyIncome - totalMontlyLoanCost - maxMontlyPayment;
    if (familySustainabilityRatio[size - 1] > actualIncome) {
      maxMontlyPayment =
        totalMontlyIncome -
        totalMontlyLoanCost -
        familySustainabilityRatio[size - 1];
    }

    return maxMontlyPayment;
  },
  mortgageDuration: (borrowers) => {
    const birthDates = [];
    for (let i = 0; i < borrowers.length; i++) {
      const age = hashUtil.computeAge(borrowers[i].birthDate);
      birthDates.push(age);
    }
    const olderBorrowerAge = Math.max.apply(Math, birthDates);

    let duration = maxLoanerAge - olderBorrowerAge;
    return duration <= 30 ? round5(duration) : 30;
  }
};
