import Joi from "joi";

export default {
  rate: {
    body: Joi.object({
      housePrice: Joi.string().required(),
      mortgage: Joi.string().required(),
      mortgageDuration: Joi.string().required(),
      cityId: Joi.string().required(),
      city: Joi.string(),
      researchStage: Joi.string(),
      fkProvince: Joi.string(),
      maxHomeValue: Joi.string().allow(null, ""),
      maxLoanAmount: Joi.string().allow(null, ""),
      maxMontlyPayment: Joi.string().allow(null, "")
    })
  }
};
