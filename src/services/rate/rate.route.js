import express from "express";
import { validate } from "express-validation";
import controller from "./rate.controller.js";
import validation from "./rate.validation.js";
import role from "../../middlewares/role.js";

const router = express.Router();

/**
 * @api {post} /v1/rates                    ask rate
 * @apiName                                 AskRates
 * @apiDescription                          Ask mortgage rate.
 * @apiGroup                                Rates
 * @apiVersion                              0.1.0
 * @apiPermission                           authenticated
 * @apiUse authenticated
 *
 * @apiBody {Number}    cityId              City ID.
 * @apiBody {Number}    housePrice          House price.
 * @apiBody {Number}    mortgage            Mortgage value.
 * @apiBody {Number}    mortgageDuration    Mortgage duration.
 *
 * @apiSuccess  {Boolean}                           success       Authorized?
 * @apiSuccess  {[Rate](#api-Models-ObjectRate)}    rate          Rate object.
 *
 */

router.post("/", validate(validation.rate), controller.onAskRate);

/**
 * @api {get} /v1/rates/:userId             get user rate quotes
 * @apiName                                 GetUserRates
 * @apiDescription                          Get rates quotes submitted from user.
 * @apiGroup                                Rates
 * @apiVersion                              0.1.0
 * @apiPermission                           admin
 * @apiUse admin
 *
 * @apiSuccess  {Boolean}                           success      Authorized?
 * @apiSuccess  {[Rate](#api-Models-ObjectRate)}    rates        List of rates
 *
 */
router.get("/:userId", role.isAdmin, controller.onListByUser);

/**
 * @api {get} /v1/rates/                    get last rate
 * @apiName                                 GetLastRate
 * @apiDescription                          Get last rate quote from survey.
 * @apiGroup                                Rates
 * @apiVersion                              0.1.0
 * @apiPermission                           authenticated
 * @apiUse authenticated
 *
 */
router.get("/", controller.onGetLastRate);

export default router;
