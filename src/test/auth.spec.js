import chai from "chai";
import chaiHttp from "chai-http";
import envar from "../config/vars.js";
import httpStatus from "http-status";
const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

//import server from "../../server.js";
const server = "127.0.0.1:3000";
/*
const sandbox = sinon.createSandbox();
import sinon from "sinon";

let defaultAdmin = {
  email: "admin",
  password: "admin@123"
};

let token;
let user;

beforeEach(async () => {
  user = {
    email: "app@mutuiamo.it"
  };
});

afterEach(() => sandbox.restore());*/

describe("AUTHENTICATION", () => {
  describe("POST v1/auth/email", () => {
    it("200 e inviare email per accesso advisor", function (done) {
      chai
        .request(server)
        .post("/v1/auth/email")
        .send({ email: "bf8b842d83c18b3c778e11825a9ff631@example.com" })
        .end((err, res) => {
          expect(res.statusCode).to.equal(httpStatus.OK);
          expect(res.body).have.property("success").and.to.equal(true);
          expect(res.body).have.property("message").and.to.be.a("string");
          done();
        });
    });
    it("404 se email non esiste", function (done) {
      chai
        .request(server)
        .post("/v1/auth/email")
        .send({
          email: "nomeutente@example.com"
        })
        .end((err, res) => {
          expect(res.statusCode).to.equal(httpStatus.NOT_FOUND);
          expect(res.body).have.property("success").and.to.equal(false);
          expect(res.body).have.property("error").and.to.be.a("string");
          done();
        });
    });
  });
  describe("GET v1/auth/token/:token", () => {
    it("401 senza apikey", function (done) {
      chai
        .request(server)
        .post("/v1/auth/token/qu3st0eun1ds8agl1at0")
        .end((err, res) => {
          expect(res.statusCode).to.equal(httpStatus.UNAUTHORIZED);
          expect(res.body).have.property("success").and.to.equal(false);
          expect(res.body).have.property("message").and.to.be.a("string");
          done();
        });
    });
    it("404 con token sbagliato", function (done) {
      chai
        .request(server)
        .post("/v1/auth/token/qu3st0eun1ds8agl1at0")
        .set("x-api-key", envar.xApiKey)
        .end((err, res) => {
          expect(res.statusCode).to.equal(httpStatus.NOT_FOUND);
          expect(res.body).have.property("success").and.to.equal(false);
          expect(res.body).have.property("error").and.to.be.a("string");
          done();
        });
    });
  });
});
