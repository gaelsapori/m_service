import chai from "chai";
import chaiHttp from "chai-http";
import envar from "../config/vars.js";
import httpStatus from "http-status";
const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);
const server = "127.0.0.1:3000";
/*
const sandbox = sinon.createSandbox();
import sinon from "sinon";

let defaultAdmin = {
  email: "admin",
  password: "admin@123"
};

let token;
let user;

beforeEach(async () => {
  user = {
    email: "app@mutuiamo.it"
  };
});

afterEach(() => sandbox.restore());*/

describe("OTP", () => {
  describe("POST v1/otp/checkphone", () => {
    it("200 per lead esportate", function (done) {
      chai
        .request(server)
        .post("/v1/otp/checkphone")
        .send({ phone: "3495154092" })
        .end((err, res) => {
          expect(res.statusCode).to.equal(httpStatus.OK);
          expect(res.body).have.property("success").and.to.equal(true);
          expect(res.body).have.property("exported").and.is.a("boolean");
          expect(res.body).have.property("verified").and.to.a("boolean");
          done();
        });
    });
    it("400 se manca il numero", function (done) {
      chai
        .request(server)
        .post("/v1/otp/checkphone")
        .end((err, res) => {
          expect(res.statusCode).to.equal(httpStatus.BAD_REQUEST);
          expect(res.body).have.property("success").and.to.equal(false);
          done();
        });
    });
  });
});
