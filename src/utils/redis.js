import redis from "redis";
import envar from "../config/vars.js";
import { promisify } from "util";
import logger from "../middlewares/logger.js";

const client = redis.createClient({
  host: envar.redis.host,
  port: envar.redis.port
  //password: envar.redis.password
});

client.on("connect", () => {
  logger.debug("[Redis] client connected");
});

client.on("error", (error) => {
  console.error(error);
});

const get = promisify(client.get).bind(client);
const set = promisify(client.set).bind(client);
const exists = promisify(client.exists).bind(client);
const del = promisify(client.del).bind(client);
const getList = promisify(client.lrange).bind(client);

export const store = client;

export default {
  get,
  set,
  exists,
  del,
  getList,
  async removeUser(socket) {
    let leaving = "";
    var list = [];
    const p = await get("users").catch((err) => {
      if (err) console.error(err);
    });

    const users = JSON.parse(p);
    const index = users.findIndex((user) => user.socketId == socket.id);
    const u = users[index];

    if (u) {
      list = users.filter(
        (user) => user.socketId !== socket.id && user.userId !== u.userId
      );
      leaving = `[${u.type}][${u.id}]`;
    } else {
      list = users.filter((user) => user.socketId !== socket.id);
    }

    await set("users", JSON.stringify(list));
    return { list, leaving };
  },
  async getUserBySocket(socket) {
    const rawUsers = await get("users").catch((err) => {
      if (err) console.error(err);
    });
    const users = JSON.parse(rawUsers);
    return users.filter((user) => user.socketId == socket.id);
  },
  async getUsers() {
    const rawUsers = await get("users").catch((err) => {
      if (err) console.error(err);
    });
    return JSON.parse(rawUsers);
  }
};
