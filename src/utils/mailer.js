import sgMail from "@sendgrid/mail";
import envar from "../config/vars.js";
import logger from "../middlewares/logger.js";

export default {
  sendVerificationEmail: async (user, token) => {
    sgMail.setApiKey(envar.sendGridToken);
    let subject = "Un click per accedere";
    let to = user.email;
    let from = envar.fromEmail;
    let link = "https://" + envar.clientUrl + "/#/verify/" + token;

    let text = `Ciao ${user.firstName}, 
                  per entrare vai su questo link:
                  \n\n ${link} \n\n. 
                  Il link scade tra 5 minuti, dopo l'accesso potrai cancellare questa mail.`;

    let html = `<p>Ciao ${user.firstName}<p>
                  <p>per entrare fai clic su questo bottone:</p>
                  <a href="${link}">Accedi</a>
                  <br><p>Il link scade tra 5 minuti, dopo l'accesso potrai cancellare questa mail.</p>`;

    await sgMail.send({ to, from, subject, text, html });
    logger.info(`[SendGrid] mail sent to advisor ${user.email} - ${token}`);
  },
  sendEmailNotificationForAppointment: async (
    appointment,
    time,
    advisor,
    user
  ) => {
    sgMail.setApiKey(envar.sendGridToken);
    let subject = `Lead ${user.leadId} - ${user.firstName} ${user.lastName} Richiesta di appuntamento`;
    let to = advisor.email;
    let from = envar.fromEmail;
    let dateobj = new Date(appointment.datetime);

    let text = `Ciao ${
      advisor.firstName
    }, è stato richiesto un appuntamento da parte di :
        \n\n ${user.firstName} ${user.lastName} \n\n. 
        \n\n Lead ID: ${user.leadId} \n\n. 
        \n\n https://gestionale.mutuiamo.it/pratiche/${user.leadId} \n\n. 
        \n\n Fase di ricerca:  ${appointment.researchStage} \n\n. 
        \n\n Banca scelta:  ${appointment.bank} \n\n. 
        \n\n Appuntamento: ${dateobj.toLocaleString("it-IT")} \n\n
        \n\n Fascia oraria: ${time} \n\n.`;

    let html = `<p>Ciao ${advisor.firstName},</p>
        <p>è stato richiesto un appuntamento da parte di 
        ${user.firstName} ${user.lastName}.</p>
        <p>Lead ID: ${user.leadId}</p>
        <p><a href="https://gestionale.mutuiamo.it/pratiche
        /${user.leadId}">Scheda CRM</a></p>
        <p>Fase di ricerca: ${appointment.researchStage}</p>
        <p>Banca scelta: ${appointment.bank}</p>
        <p>Appuntamento: ${dateobj.toLocaleString("it-IT")}</p>
        <p>Fascia oraria: ${time}</p>`;

    await sgMail.send({ to, from, subject, text, html });
    logger.info(
      `[SendGrid] mail sent to advisor ${advisor.email} - ${user.firstName} ${user.lastName}`
    );
    return true;
  },
  sendEmailOtp: async (email, otp) => {
    sgMail.setApiKey(envar.sendGridToken);

    let subject = `Il tuo codice è: ${otp}`;
    let to = email;
    let from = envar.fromEmail;
    let text = `Ecco il tuo codice di sicurezza Mutuiamo: ${otp}. Scade tra 5 minuti.\n\n`;
    let html = `<p>Ecco il tuo codice di sicurezza Mutuiamo: ${otp}. Scade tra 5 minuti.</p>`;

    await sgMail.send({ to, from, subject, text, html });
    logger.info(`[SendGrid] mail sent to ${email} - ${otp}`);
    return true;
  }
};
