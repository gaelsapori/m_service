import Message from "../services/room/message.model.js";
import User from "../services/user/user.model.js";
import redis from "../utils/redis.js";
import logger from "../middlewares/logger.js";

class Socket {
  connection(socket) {
    // event fired when the chat room is disconnected
    socket.on("disconnect", async () => {
      if ((await redis.getUsers()) != null) {
        var u = await redis.removeUser(socket);
        var advisors = u.list.filter((u) => u.userId && u.type == "advisor");
        var customers = u.list.filter((u) => u.userId && u.type == "customer");
        logger.info(
          `[socket.io] disconnect ${u.leaving} - onlineAdvisors: ${advisors.length} onlineUsers: ${customers.length}`
        );

        const userList = u.list.map((u) => u.userId);
        global.io.sockets.emit("appreance", userList);
      }
    });
    // add identity of user mapped to the socket id
    socket.on("identity", async (user) => {
      var list = await redis.getUsers();
      if (!list) list = [];
      let u = { socketId: socket.id };
      if (typeof user === "string" || user instanceof String) {
        const mUser = await User.get(user);
        u.userId = user;
        u.type = mUser.type;
        u.id = mUser.leadId || mUser.advisorId;
      } else {
        u.userId = user.id ? user.id : user._id;
        u.type = user.type;
        u.id = user.leadId || user.advisorId;
      }
      list.push(u);
      await redis.set("users", JSON.stringify(list));

      const advisors = list.filter((u) => u.userId && u.type == "advisor");
      const customers = list.filter((u) => u.userId && u.type == "customer");
      logger.info(
        `[socket.io] connect [${u.type}][${u.id}] - onlineAdvisors: ${advisors.length} onlineUsers: ${customers.length}`
      );
      global.io.sockets.emit(
        "appreance",
        list.map((u) => u.userId)
      );
    });
    // listening to room event
    socket.on("subscribe", (room) => {
      //logger.debug(`[socket.io] subscribing room ${room}`);
      socket.join(room);
    });
    // user is typping in chat
    socket.on("typing", (data) => {
      socket.broadcast.emit("typing", data);
    });
    // use read the conversation
    socket.on("ack", async (roomId, userId) => {
      const result = await Message.markMessageRead(roomId, userId);
      //emit to all in room except sender
      socket.broadcast.to(roomId).emit("ack", {
        result: result,
        readByRecipient: {
          readAt: new Date().toISOString(),
          readBy: userId
        }
      });
    });
  }
}

export default new Socket();
