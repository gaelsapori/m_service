import crypto from "crypto";
import voucher_codes from "voucher-code-generator";
import envar from "../config/vars.js";

const makeHash = (data, type = "sha1") =>
  crypto.createHmac(type, envar.hashSecret).update(data).digest("hex");

export default {
  encrypt: (data) => {
    return makeHash(data, "sha1");
  },
  otp: (data) => {
    return makeHash(data, "sha256");
  },
  generate: (phone, otp) => {
    const ttl = 5 * 60 * 1000;
    const expires = Date.now() + ttl;
    const data = `${phone}.${otp}.${expires}`;
    const hash = makeHash(data, "sha256");
    return `${hash}.${expires}`;
  },
  emailMask: (email) => {
    let split = email.split("@");
    let masked = [...split[0]]
      .map((v, i) => ((i + 1) % 2 == 0 ? "*" : v))
      .join("");
    return masked + "@" + split[1];
  },
  computeAge: (dateString) => {
    if (!dateString) return null;
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  },
  referralCode: () => {
    const code = voucher_codes.generate({
      length: 7,
      charset: "123456789ABCDEFGHIJKLMNPQRSTUVXYZ"
    });
    return code[0];
  }
};
