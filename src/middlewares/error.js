import httpStatus from "http-status";
import { ValidationError } from "express-validation";
import jwt from "jsonwebtoken";
const { JsonWebTokenError, TokenExpiredError } = jwt;
import APIError from "../utils/api.error.js";
import envar from "../config/vars.js";
import logger from "../middlewares/logger.js";

export const handler = (err, req, res, next) => {
  logger.error(`[${err.status}] ${err.message || httpStatus[err.status]}`);
  const response = {
    success: false,
    message: err.message || httpStatus[err.status]
    //code: err.status,
    //errors: err.errors
    //stack: err.stack
  };
  if (envar.env !== "development") {
    delete response.stack;
    delete response.errors;
  }

  res.status(err.status ?? httpStatus.INTERNAL_SERVER_ERROR);
  res.json(response);
  res.end();
};

export const converter = (err, req, res, next) => {
  let convertedError = err;

  if (err instanceof ValidationError) {
    let errors = [];
    for (const [key, _] of Object.entries(err.details)) {
      errors = [
        ...errors,
        ...err.details[key].map((e) => ({
          location: key,
          message: e.message,
          type: e.type,
          field: e.path[0]
        }))
      ];
    }
    convertedError = new APIError({
      message: "Errore di validazione",
      errors,
      status: err.status ?? err.statusCode,
      stack: err.stack
    });
  } else if (err instanceof JsonWebTokenError) {
    convertedError = new APIError({
      message: err.message,
      status: httpStatus.FORBIDDEN
    });
    if (err instanceof TokenExpiredError)
      convertedError.message = "Token scaduto";
  } else if (!(err instanceof APIError)) {
    convertedError = new APIError({
      message: err.message,
      status: err.status ?? err.statusCode,
      stack: err.stack
    });
  }
  return handler(convertedError, req, res);
};

export const notFound = (req, res, next) => {
  const err = new APIError({
    message: "Rotta sconosciuta.",
    status: httpStatus.NOT_FOUND
  });
  return handler(err, req, res);
};

export const rateLimitHandler = (req, res, next) => {
  const err = new APIError({
    message: "Limite di richieste superato, riprovare più tardi.",
    status: httpStatus.TOO_MANY_REQUESTS
  });
  return handler(err, req, res);
};
