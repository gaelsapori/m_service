import rateLimit from "express-rate-limit";
import envar from "../config/vars.js";
import RedisStore from "rate-limit-redis";
import { store } from "../utils/redis.js";
import { rateLimitHandler } from "../middlewares/error.js";

const requestTimeSpan = parseInt(envar.rateLimit.seconds);
const otpLimit = parseInt(envar.rateLimit.otp);
const authLimit = parseInt(envar.rateLimit.auth);

const redisStore = (prefix) =>
  new RedisStore({
    client: store,
    expiry: requestTimeSpan,
    prefix: prefix
  });

export const phoneOtpLimiter = rateLimit({
  store: redisStore("phone_otp"),
  max: otpLimit,
  handler: rateLimitHandler
});
export const emailOtpLimiter = rateLimit({
  store: redisStore("email_otp"),
  max: otpLimit,
  handler: rateLimitHandler
});
export const verifyOtpLimiter = rateLimit({
  store: redisStore("verify_otp"),
  max: otpLimit,
  handler: rateLimitHandler
});
export const checkPhoneLimiter = rateLimit({
  store: redisStore("check_phone"),
  max: otpLimit,
  handler: rateLimitHandler
});
export const authLimiter = rateLimit({
  store: redisStore("auth"),
  max: authLimit,
  handler: rateLimitHandler
});
