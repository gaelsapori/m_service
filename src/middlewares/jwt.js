import jwt from "jsonwebtoken";
import envar from "../config/vars.js";
import User from "../services/user/user.model.js";
import httpStatus from "http-status";
import APIError from "../utils/api.error.js";
import { handler as errorHandler } from "../middlewares/error.js";

export const encode = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const user = await User.get(userId);
    const payload = {
      userId: user._id,
      userType: user.type
    };
    const authToken = jwt.sign(payload, envar.jwtSecret);
    req.authToken = authToken;
    next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

export const decode = (req, res, next) => {
  try {
    if (!req.headers["authorization"]) {
      throw new APIError({
        message: "Accesso negato",
        status: httpStatus.UNAUTHORIZED
      });
    }
    const accessToken = req.headers.authorization.split(" ");
    if (accessToken[0] !== "Bearer") {
      throw new APIError({
        message: "Accesso negato",
        status: httpStatus.UNAUTHORIZED
      });
    }
    const decoded = jwt.verify(accessToken[1], envar.jwtSecret);
    req.userId = decoded.userId;
    req.userType = decoded.userType;
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

export const apikey = (req, res, next) => {
  try {
    if (req.headers["x-api-key"] != envar.xApiKey) {
      throw new APIError({
        message: "Accesso negato",
        status: httpStatus.UNAUTHORIZED
      });
    }
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};
