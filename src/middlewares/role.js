const denyResponse = { success: false, message: "Non sei autorizzato." };
import { handler as errorHandler } from "../middlewares/error.js";

const checkRole = async (req, res, next, allowedRoles) => {
  try {
    if (allowedRoles.includes(req.userType)) {
      next();
    } else {
      res.status(403).json(denyResponse);
    }
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

export default {
  isAdmin: (req, res, next) => {
    try {
      checkRole(req, res, next, ["admin"]);
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  isEditor: (req, res, next) => {
    try {
      checkRole(req, res, next, ["admin", "editor"]);
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  isAdvisor: (req, res, next) => {
    try {
      checkRole(req, res, next, ["advisor"]);
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  isAdvisorOrAdmin: (req, res, next) => {
    try {
      checkRole(req, res, next, ["admin", "advisor"]);
    } catch (error) {
      return errorHandler(error, req, res);
    }
  },
  isCustomer: (req, res, next) => {
    try {
      checkRole(req, res, next, ["customer"]);
    } catch (error) {
      return errorHandler(error, req, res);
    }
  }
};
