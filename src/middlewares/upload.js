import { GridFsStorage } from "multer-gridfs-storage";
import crypto from "crypto";
import path from "path";
import multer from "multer";
import envar from "../config/vars.js";
import Room from "../services/room/room.model.js";
import httpStatus from "http-status";
import APIError from "../utils/api.error.js";

const storageFn = (prefix, bucket, onwership, fromAdvisor) =>
  new GridFsStorage({
    url: envar.mongo.uri,
    file: (req, file) => {
      return new Promise((resolve, reject) => {
        crypto.randomBytes(16, (err, buf) => {
          (async () => {
            if (err) {
              return reject(err);
            }
            const filename =
              prefix + buf.toString("hex") + path.extname(file.originalname);
            const fileInfo = {
              filename: filename,
              bucketName: bucket
            };
            const metadata = {
              userId: req.userId,
              userType: req.userType
            };
            if (fromAdvisor) {
              const roomId = req.params.roomId;
              const room = await Room.getRoomRecipient(roomId, req.userId);
              if (!room) {
                return reject(err);
              }
              metadata.userId = room.recipient.id;
              metadata.userType = room.recipient.type;
            }
            if (onwership) fileInfo.metadata = metadata;
            resolve(fileInfo);
          })();
        });
      });
    }
  });

export const image = multer({
  storage: storageFn("mutuiamo_", "images", false, false)
});
export const pdf = multer({
  storage: storageFn("document_", "documents", true, true),
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);
    if (ext !== ".pdf") {
      return callback(
        new APIError({
          message: "Non è un file di tipo pdf.",
          status: httpStatus.BAD_REQUEST
        })
      );
    }
    callback(null, true);
  }
});
export const document = multer({
  storage: storageFn("document_", "documents", true, false),
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);
    if (ext !== ".png" && ext !== ".jpg" && ext !== ".gif" && ext !== ".jpeg") {
      return callback(
        new APIError({
          message: "Non è un file di tipo jpg o png.",
          status: httpStatus.BAD_REQUEST
        })
      );
    }
    callback(null, true);
  }
});
