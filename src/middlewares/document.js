import crypto from "crypto";
import path from "path";
import multer from "multer";
import { GridFsStorage } from "multer-gridfs-storage";
import envar from "../config/vars.js";

const storage = new GridFsStorage({
  url: envar.mongo.uri,
  file: (req, file) => {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        const currentLoggedUser = req.userId;
        const currentLoggedUserType = req.userType;
        const filename =
          "document_" + buf.toString("hex") + path.extname(file.originalname);
        const fileInfo = {
          filename: filename,
          bucketName: "documents",
          metadata: {
            userId: currentLoggedUser,
            userType: currentLoggedUserType
          }
        };
        resolve(fileInfo);
      });
    });
  }
});

export const document = multer({ storage });
