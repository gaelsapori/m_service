import envar from "./config/vars.js";
import app from "./config/express.js";
import mongoose from "./config/mongoose.js";
import http from "http";
import { Server } from "socket.io";
import logger from "./middlewares/logger.js";
import socket from "./utils/socket.js";

mongoose.connect();

const server = http.createServer(app);
global.io = new Server(server);
global.io.on("connection", socket.connection);

server.listen(envar.port, () => {
  logger.debug(`[Node] server started on port ${envar.port} (${envar.env})`);
});

export default app;
